Step1: Download maven
    Download maven : https://mirror.downloadvn.com/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
    Unzip it and move to the folder you wan to
Step2: Setting environment
    Download JDk: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
    Open Environment Variables
    Add to System variables
        JAVA_HOME : ...\Java\jdk-11.0.6
        M2_HOME : ...\apache-maven-3.6.3
    Add to Path
        %JAVA_HOME%\bin
        %M2_HOME%\bin
Step3: Install Intellij
    Download: https://www.jetbrains.com/idea/download
Step4: Open the project
    Clone project : git clone https://LinhHNTGCH16483@bitbucket.org/LinhHNTGCH16483/etutoring.git
    Open project as maven project
Step5: Setting Intellij
    Open Setting (Ctrl + Alt + S)
    Search for "maven" setting
    Maven home dictionary : browse to ...\apache-maven-3.6.3
    User settings file :browse to ...\apache-maven-3.6.3\conf\settings.xml
    Search for "plugins"
    Search and Install "Lombok"
Step6: Generate database
    Download and Install Mariadb: https://downloads.mariadb.com/MariaDB/mariadb-10.4.12/winx64-packages/mariadb-10.4.12-winx64.msi
    Open and create new session
        Hostname/IP : 127.0.0.1
        User : root
        Password: Linhhntgch16483
        Port : 3306
Step7: Run the project