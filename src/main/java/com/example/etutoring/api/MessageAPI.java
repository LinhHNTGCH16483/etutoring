package com.example.etutoring.api;

import com.example.etutoring.domain.dto.MessageDto;
import com.example.etutoring.domain.dto.MessageReportDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/messages")
public interface MessageAPI {

    @PostMapping(value = "")
    Boolean saveMessage(@RequestBody MessageDto messageDto);

    @GetMapping(value = "")
    List<MessageReportDto> getStudentMessage();

    @GetMapping(value = "/7day")
    Long countMessageIn7Day();

    @GetMapping(value = "/average-tutor")
    Long averageTutorMessage();

    @GetMapping(value = "/average-student")
    Long averageStudentMessage();

    @GetMapping(value = "", params = {"classId"})
    List<MessageDto> getMessageByClass(@RequestParam("classId") Long classId);

    @GetMapping(value = "total-student-msg")
    Long countStudentMessage(@RequestParam("studentId") String studentId);

    @GetMapping(value = "total-tutor-msg")
    Long countTutorMessage(@RequestParam("tutorId") String tutorId);
}
