package com.example.etutoring.api;

import com.example.etutoring.domain.dto.CalendarDto;
import com.example.etutoring.domain.dto.ScheduleDto;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/schedules")
public interface ScheduleAPI {

    @PostMapping(value = "")
    Boolean createMeeting(@RequestBody ScheduleDto scheduleDto);

    @GetMapping(value = "", params = {"studentId", "fromDate", "toDate"})
    List<CalendarDto> getStudentSchedule(@RequestParam("studentId") String studentId,
                                         @RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                         @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);

    @GetMapping(value = "", params = {"tutorId", "fromDate", "toDate"})
    List<CalendarDto> getTutorSchedule(@RequestParam("tutorId") String tutorId,
                                       @RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                       @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);
}
