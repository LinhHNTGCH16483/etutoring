package com.example.etutoring.api;

import com.example.etutoring.domain.dto.StaffAccountDto;
import com.example.etutoring.domain.dto.StaffDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/staffs")
public interface StaffAPI {

    @GetMapping(value = "")
    List<StaffAccountDto> getAllStaff();

    @GetMapping(value = "", params = {"id"})
    StaffDto getStaffById(@RequestParam("id") String staffId);

    @GetMapping(value = "", params = {"username"})
    StaffDto getStaffByUsername(@RequestParam("username") String username);

}
