package com.example.etutoring.api;

import com.example.etutoring.domain.dto.TutorAccountDto;
import com.example.etutoring.domain.dto.TutorDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/tutors")
public interface TutorAPI {

    @GetMapping(value = "")
    List<TutorAccountDto> getAllTutor();

    @GetMapping(value = "", params = {"id"})
    TutorDto getTutorById(@RequestParam("id") String tutorId);

    @GetMapping(value = "/students",params = {"id"})
    Long getStudentNumber(@RequestParam("id") String tutorId);

    @GetMapping(value = "", params = {"username"})
    TutorDto getTutorByUsername(@RequestParam("username") String username);

    @GetMapping(value = "/total")
    Long getNumberOfTutor();
}
