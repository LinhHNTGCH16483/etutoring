package com.example.etutoring.api;

import com.example.etutoring.domain.dto.FileDto;
import com.example.etutoring.domain.dto.FileResponeDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/files")
public interface FileAPI {

    @GetMapping(value = "", params = {"classId"})
    List<FileDto> getFileByClass(@RequestParam("classId") Long classId);

    @PostMapping(value = "", params = {"classId", "file"})
    FileResponeDto uploadFile(@RequestParam("classId") Long classId, @RequestParam("file") MultipartFile file);

    @PostMapping(value = "", params = {"classId", "files"})
    List<FileResponeDto> uploadFiles(@RequestParam("classId") Long classId, @RequestParam("files") MultipartFile[] files);

    @GetMapping(value = "", params = {"fileName"})
    ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName, HttpServletRequest httpServletRequest);
}
