package com.example.etutoring.api;

import com.example.etutoring.domain.dto.AccountDto;
import com.example.etutoring.domain.dto.LoginDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:4200")
public interface AccountAPI {

    @PostMapping(value = "/login")
    AccountDto login(@RequestBody LoginDto loginDto);
}
