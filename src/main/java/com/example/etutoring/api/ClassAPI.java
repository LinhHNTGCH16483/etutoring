package com.example.etutoring.api;

import com.example.etutoring.domain.dto.ClassDto;
import com.example.etutoring.domain.dto.ClassInforDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/classes")
public interface ClassAPI {

    @GetMapping(value = "")
    List<ClassInforDto> getAllClass();

    @GetMapping(value = "/students",params = {"studentId"})
    ClassInforDto getClassByStudent(@RequestParam("studentId") String studentId);

    @GetMapping(value = "/tutors", params = "tutorId")
    List<ClassInforDto> getClassByTutor(@RequestParam("tutorId") String tutorId);

    @PostMapping(value = "")
    boolean saveClasses(@RequestBody List<ClassDto> classDtoList);

    @PutMapping(value = "")
    boolean updateClass(@RequestBody ClassDto classDto);
}
