package com.example.etutoring.api;

import com.example.etutoring.domain.dto.StudentAccountDto;
import com.example.etutoring.domain.dto.StudentDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/students")
public interface StudentAPI {

    @GetMapping(value = "")
    List<StudentAccountDto> getAllStudent();

    @GetMapping(value = "", params = {"id"})
    StudentDto getStudentById(@RequestParam("id") String studentId);

    @GetMapping(value = "", params = {"username"})
    StudentDto getStudentByUsername(@RequestParam("username") String username);

    @GetMapping(value = "/total")
    Long getNumberOfStudent();

    @GetMapping(value = "/undeclared")
    List<StudentAccountDto> getStudentNoTutor();

    @GetMapping(value = "/total-undeclared")
    Long getNumberOfStudentNoTutor();

    @GetMapping(value = "/no-interaction", params = {"days"})
    List<StudentAccountDto> getStudentNoInteraction(@RequestParam("days") Long day);
}
