package com.example.etutoring.controller;

import com.example.etutoring.api.MessageAPI;
import com.example.etutoring.domain.dto.MessageDto;
import com.example.etutoring.domain.dto.MessageReportDto;
import com.example.etutoring.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageController implements MessageAPI {

    @Autowired
    MessageService messageService;

    @Override
    public Boolean saveMessage(MessageDto messageDto) {
        return messageService.saveMessage(messageDto);
    }

    @Override
    public List<MessageReportDto> getStudentMessage() {
        return messageService.getStudentMessage();
    }

    @Override
    public Long countMessageIn7Day() {
        return messageService.countMessageIn7Day();
    }

    @Override
    public Long averageTutorMessage() {
        return messageService.averageTutorMessage();
    }

    @Override
    public Long averageStudentMessage() {
        return messageService.averageStudentMessage();
    }

    @Override
    public List<MessageDto> getMessageByClass(Long classId) {
        return messageService.getMessageByClass(classId);
    }

    @Override
    public Long countStudentMessage(String studentId) {
        return messageService.countStudentMessage(studentId);
    }

    @Override
    public Long countTutorMessage(String tutorId) {
        return messageService.countTutorMessage(tutorId);
    }
}
