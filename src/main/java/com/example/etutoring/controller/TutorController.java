package com.example.etutoring.controller;

import com.example.etutoring.api.TutorAPI;
import com.example.etutoring.domain.dto.TutorAccountDto;
import com.example.etutoring.domain.dto.TutorDto;
import com.example.etutoring.service.TutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TutorController implements TutorAPI {

    @Autowired
    TutorService tutorService;

    @Override
    public List<TutorAccountDto> getAllTutor() {
        return tutorService.getAllTutorInfor();
    }

    @Override
    public TutorDto getTutorById(String tutorId) {
        return tutorService.getTutorById(tutorId);
    }

    @Override
    public Long getStudentNumber(String tutorId) {
        return tutorService.getStudentNumber(tutorId);
    }

    @Override
    public TutorDto getTutorByUsername(String username) {
        return tutorService.getTutorByUsername(username);
    }

    @Override
    public Long getNumberOfTutor() {
        return tutorService.getNumberOfTutor();
    }
}
