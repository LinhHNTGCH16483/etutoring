package com.example.etutoring.controller;

import com.example.etutoring.api.ScheduleAPI;
import com.example.etutoring.domain.dto.CalendarDto;
import com.example.etutoring.domain.dto.ScheduleDto;
import com.example.etutoring.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class ScheduleController implements ScheduleAPI {

    @Autowired
    ScheduleService scheduleService;

    @Override
    public Boolean createMeeting(ScheduleDto scheduleDto) {
        return scheduleService.createMeeting(scheduleDto);
    }

    @Override
    public List<CalendarDto> getStudentSchedule(String studentId, LocalDate fromDate, LocalDate toDate) {
        return scheduleService.getPersonSchedule(studentId, null, fromDate, toDate);
    }

    @Override
    public List<CalendarDto> getTutorSchedule(String tutorId, LocalDate fromDate, LocalDate toDate) {
        return scheduleService.getPersonSchedule(null, tutorId, fromDate, toDate);
    }
}
