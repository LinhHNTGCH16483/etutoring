package com.example.etutoring.controller;

import com.example.etutoring.api.FileAPI;
import com.example.etutoring.domain.dto.FileDto;
import com.example.etutoring.domain.dto.FileResponeDto;
import com.example.etutoring.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class FileController implements FileAPI {

    @Autowired
    FileService fileService;

    @Override
    public List<FileDto> getFileByClass(Long classId) {
        return fileService.getFileByClass(classId);
    }

    @Override
    public FileResponeDto uploadFile(Long classId, MultipartFile file) {
        return fileService.storeFile(classId, file);
    }

    @Override
    public List<FileResponeDto> uploadFiles(Long classId, MultipartFile[] files) {
        return fileService.storeFiles(classId, files);
    }

    @Override
    public ResponseEntity<Resource> downloadFile(String fileName, HttpServletRequest httpServletRequest) {
        return fileService.loadFile(fileName, httpServletRequest);
    }
}
