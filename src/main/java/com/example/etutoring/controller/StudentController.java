package com.example.etutoring.controller;

import com.example.etutoring.api.StudentAPI;
import com.example.etutoring.domain.dto.StudentAccountDto;
import com.example.etutoring.domain.dto.StudentDto;
import com.example.etutoring.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController implements StudentAPI {

    @Autowired
    StudentService studentService;

    @Override
    public List<StudentAccountDto> getAllStudent() {
        return studentService.getAllStudentInfor();
    }

    @Override
    public StudentDto getStudentById(String studentId) {
        return studentService.getStudentById(studentId);
    }

    @Override
    public StudentDto getStudentByUsername(String username) {
        return studentService.getStudentByUsername(username);
    }

    @Override
    public Long getNumberOfStudent() {
        return studentService.getNumberOfStudent();
    }

    @Override
    public List<StudentAccountDto> getStudentNoTutor() {
        return studentService.getStudentNoTutor();
    }

    @Override
    public Long getNumberOfStudentNoTutor() {
        return studentService.getNumberOfStudentNoTutor();
    }

    @Override
    public List<StudentAccountDto> getStudentNoInteraction(Long day) {
        return studentService.getStudentNoInteraction(day);
    }
}
