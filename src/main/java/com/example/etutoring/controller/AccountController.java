package com.example.etutoring.controller;

import com.example.etutoring.api.AccountAPI;
import com.example.etutoring.domain.dto.AccountDto;
import com.example.etutoring.domain.dto.LoginDto;
import com.example.etutoring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController implements AccountAPI {

    @Autowired
    AccountService accountService;

    @Override
    public AccountDto login(LoginDto loginDto) {
        return accountService.findAccountByUsernameAndPassword(loginDto);
    }
}
