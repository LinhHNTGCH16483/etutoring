package com.example.etutoring.controller;

import com.example.etutoring.api.ClassAPI;
import com.example.etutoring.domain.dto.ClassDto;
import com.example.etutoring.domain.dto.ClassInforDto;
import com.example.etutoring.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClassController implements ClassAPI {

    @Autowired
    ClassService classService;

    @Override
    public List<ClassInforDto> getAllClass() {
        return classService.getAllClassInfor();
    }

    @Override
    public ClassInforDto getClassByStudent(String studentId) {
        return classService.getClassByStudent(studentId);
    }

    @Override
    public List<ClassInforDto> getClassByTutor(String tutorId) {
        return classService.getClassByTutor(tutorId);
    }

    @Override
    public boolean saveClasses(List<ClassDto> classDtoList) {
        return classService.addClasses(classDtoList);
    }

    @Override
    public boolean updateClass(ClassDto classDto) {
        return classService.updateClass(classDto);
    }
}
