package com.example.etutoring.controller;

import com.example.etutoring.api.StaffAPI;
import com.example.etutoring.domain.dto.StaffAccountDto;
import com.example.etutoring.domain.dto.StaffDto;
import com.example.etutoring.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StaffController implements StaffAPI {

    @Autowired
    StaffService staffService;

    @Override
    public List<StaffAccountDto> getAllStaff() {
        return staffService.getAllStaffInfor();
    }

    @Override
    public StaffDto getStaffByUsername(String username) {
        return staffService.getStaffByUsername(username);
    }

    @Override
    public StaffDto getStaffById(String staffId) {
        return staffService.getStaffById(staffId);
    }
}
