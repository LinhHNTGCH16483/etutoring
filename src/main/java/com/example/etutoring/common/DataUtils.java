package com.example.etutoring.common;

import com.example.etutoring.domain.dto.CalendarDto;
import com.example.etutoring.domain.dto.ScheduleDto;
import com.example.etutoring.domain.dto.SlotDto;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class DataUtils {

    public static String encrypt(String str) {
        String result = "";
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(str.getBytes());
            BigInteger bigInteger = new BigInteger(1, digest.digest());
            result = bigInteger.toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Long parseToLong(Object obj) {
        if (ObjectUtils.isNull(obj)) {
            return null;
        }
        return Long.valueOf(String.valueOf(obj));
    }

    public static String parseToString(Object obj) {
        if (ObjectUtils.isNull(obj)) {
            return null;
        }
        return String.valueOf(obj);
    }

    public static Boolean parseToBoolean(Object obj) {
        if (ObjectUtils.isNull(obj)) {
            return false;
        }
        return Boolean.valueOf(String.valueOf(obj));
    }

    public static LocalDate parseToLocalDate(Object obj) {
        if (ObjectUtils.isNull(obj)) {
            return null;
        }
        return LocalDate.parse(String.valueOf(obj));
    }

    public static LocalDateTime parseToLocalDateTime(Object obj) {
        if (ObjectUtils.isNull(obj)) {
            return null;
        }
        return LocalDateTime.parse(String.valueOf(obj).replace(" ", "T"));
    }

    public static CalendarDto mergeSchedule(CalendarDto calendarDto, ScheduleDto scheduleDto) {
        return CalendarDto.builder()
                .date(calendarDto.getDate())
                .slot1(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot1().getId(), scheduleDto.getId(), calendarDto.getSlot1().getSlot(), scheduleDto.getSlot1()))
                        .slot(mergeSlot(calendarDto.getSlot1().getSlot(), scheduleDto.getSlot1()))
                        .build())
                .slot2(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot2().getId(), scheduleDto.getId(), calendarDto.getSlot2().getSlot(), scheduleDto.getSlot2()))
                        .slot(mergeSlot(calendarDto.getSlot2().getSlot(), scheduleDto.getSlot2()))
                        .build())
                .slot3(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot3().getId(), scheduleDto.getId(), calendarDto.getSlot3().getSlot(), scheduleDto.getSlot3()))
                        .slot(mergeSlot(calendarDto.getSlot3().getSlot(), scheduleDto.getSlot3()))
                        .build())
                .slot4(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot4().getId(), scheduleDto.getId(), calendarDto.getSlot4().getSlot(), scheduleDto.getSlot4()))
                        .slot(mergeSlot(calendarDto.getSlot4().getSlot(), scheduleDto.getSlot4()))
                        .build())
                .slot5(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot5().getId(), scheduleDto.getId(), calendarDto.getSlot5().getSlot(), scheduleDto.getSlot5()))
                        .slot(mergeSlot(calendarDto.getSlot5().getSlot(), scheduleDto.getSlot5()))
                        .build())
                .slot6(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot6().getId(), scheduleDto.getId(), calendarDto.getSlot6().getSlot(), scheduleDto.getSlot6()))
                        .slot(mergeSlot(calendarDto.getSlot6().getSlot(), scheduleDto.getSlot6()))
                        .build())
                .slot7(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot7().getId(), scheduleDto.getId(), calendarDto.getSlot7().getSlot(), scheduleDto.getSlot7()))
                        .slot(mergeSlot(calendarDto.getSlot7().getSlot(), scheduleDto.getSlot7()))
                        .build())
                .slot8(SlotDto.builder()
                        .id(mergeId(calendarDto.getSlot8().getId(), scheduleDto.getId(), calendarDto.getSlot8().getSlot(), scheduleDto.getSlot8()))
                        .slot(mergeSlot(calendarDto.getSlot8().getSlot(), scheduleDto.getSlot8()))
                        .build())
                .build();
    }

    private static Boolean mergeSlot(Boolean a, Boolean b) {
        return a || b;
    }

    private static String mergeId(String str1, String str2, boolean slot1, boolean slot2) {
        if (slot1 || slot2) {
            return ObjectUtils.isNull(str2) ? str1 : str2;
        }
        return null;
    }
}
