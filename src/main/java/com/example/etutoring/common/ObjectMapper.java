package com.example.etutoring.common;

import com.example.etutoring.domain.dto.*;
import com.example.etutoring.domain.entity.Class;
import com.example.etutoring.domain.entity.*;

public class ObjectMapper {

    public static AccountDto mapToAccountDto(Account account) {
        if (ObjectUtils.isNull(account)) {
            return null;
        }
        return AccountDto.builder()
                .username(account.getUsername())
                .password(account.getPassword())
                .type(account.getType())
                .lastLogin(account.getLastLogin())
                .build();
    }

    public static StudentDto mapToStudentDto(Student student) {
        if (ObjectUtils.isNull(student)) {
            return null;
        }
        return StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .email(student.getEmail())
                .dateOfBirth(student.getDateOfBirth())
                .phoneNumber(student.getPhoneNumber())
                .address(student.getAddress())
                .avatar(student.getAvatar())
                .username(student.getUsername())
                .build();
    }

    public static TutorDto mapToTutorDto(Tutor tutor) {
        if (ObjectUtils.isNull(tutor)) {
            return null;
        }
        return TutorDto.builder()
                .id(tutor.getId())
                .name(tutor.getName())
                .email(tutor.getEmail())
                .dateOfBirth(tutor.getDateOfBirth())
                .phoneNumber(tutor.getPhoneNumber())
                .address(tutor.getAddress())
                .avatar(tutor.getAvatar())
                .username(tutor.getUsername())
                .build();
    }

    public static StaffDto mapToStaffDto(Staff staff) {
        if (ObjectUtils.isNull(staff)) {
            return null;
        }
        return StaffDto.builder()
                .id(staff.getId())
                .name(staff.getName())
                .email(staff.getEmail())
                .dateOfBirth(staff.getDateOfBirth())
                .phoneNumber(staff.getPhoneNumber())
                .address(staff.getAddress())
                .avatar(staff.getAvatar())
                .username(staff.getUsername())
                .build();
    }

    public static CalendarDto mapToCalendarDto(ScheduleDto scheduleDto) {
        return CalendarDto.builder()
                .date(scheduleDto.getDate())
                .slot1(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot1())
                        .build())
                .slot2(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot2())
                        .build())
                .slot3(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot3())
                        .build())
                .slot4(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot4())
                        .build())
                .slot5(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot5())
                        .build())
                .slot6(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot6())
                        .build())
                .slot7(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot7())
                        .build())
                .slot8(SlotDto.builder()
                        .id(scheduleDto.getId())
                        .slot(scheduleDto.getSlot8())
                        .build())
                .build();
    }

    public static ClassDto mapToClassDto(Class _class) {
        return ClassDto.builder()
                .classId(_class.getId())
                .studentId(_class.getStudentId())
                .tutorId(_class.getTutorId())
                .build();
    }
}
