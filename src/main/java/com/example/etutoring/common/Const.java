package com.example.etutoring.common;

public class Const {

    public static final class Email {
        public static final String ACCOUNT = "ETutoring.PleaseDoNotReply@gmail.com";
        public static final String PASSWORD = "!QAZxsw2#EDC";
        public static final String SUBJECT = "[E-Tutoring]";
    }

    public static final class DateTimeFormat {
        public static final String DateTime_Format = "dd/MM/yyyy hh:mm:ss";
        public static final String Date_Format = "dd/MM/yyyy";
        public static final String Time_Format = "dd/MM/yyyy";
    }

    public static final class AccountType {
        public static final String STUDENT = "student";
        public static final String TUTOR = "tutor";
        public static final String STAFF = "staff";
    }
}
