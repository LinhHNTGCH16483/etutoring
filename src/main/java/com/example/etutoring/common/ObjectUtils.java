package com.example.etutoring.common;

public class ObjectUtils {
    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean notNull(Object obj) {
        return !isNull(obj);
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null && str.equals("");
    }

    public static boolean notNullOrEmpty(String str) {
        return !isNullOrEmpty(str);
    }
}
