package com.example.etutoring.repository;

import com.example.etutoring.domain.dto.MessageReportDto;

import java.util.List;

public interface MessageRepoCustom {

    List<MessageReportDto> getStudentMessage();

    List<?> getMessageIn7Day();

    List<?> getMessageByClass(Long classId);
}
