package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Class;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepo extends ClassRepoCustom, JpaRepository<Class, Long> {

    @Modifying
    @Query(value = "INSERT INTO class (class_id, student_id, tutor_id) VALUES (null, ?1, ?2) ;", nativeQuery = true)
    int insertClass(String studentId, String tutorId);

    @Modifying
    @Query(value = "UPDATE class c SET c.student_id = ?1, c.tutor_id = ?2 WHERE c.class_id= ?3 ;", nativeQuery = true)
    int updateClass(String studentId, String tutorId, Long classId);
}
