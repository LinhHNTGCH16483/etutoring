package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface ScheduleRepo extends ScheduleRepoCustom, JpaRepository<Schedule, Long> {

    @Modifying
    @Query(value = "INSERT INTO schedule( class_id, date, slot_1, slot_2, slot_3, slot_4, slot_5, slot_6, slot_7, slot_8) " +
            "VALUES ( ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10 );", nativeQuery = true)
    int createMeeting(Long classId, LocalDate date, Boolean slot1, Boolean slot2,
                      Boolean slot3, Boolean slot4, Boolean slot5, Boolean slot6, Boolean slot7, Boolean slot8);
}
