package com.example.etutoring.repository;

import com.example.etutoring.domain.dto.ScheduleDto;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleRepoCustom {
    List<ScheduleDto> getPersonSchedule(String studentId, String tutorId, LocalDate fromDate, LocalDate toDate);
}
