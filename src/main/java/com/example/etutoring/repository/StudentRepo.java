package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends StudentRepoCustom, JpaRepository<Student, String> {

    Student findByUsername(String username);
}

