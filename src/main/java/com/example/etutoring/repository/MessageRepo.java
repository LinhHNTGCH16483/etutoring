package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface MessageRepo extends MessageRepoCustom, JpaRepository<Message, Long> {

    @Modifying
    @Query(value = "INSERT INTO message ( class_id, time, content, sender ) VALUES ( ?1, ?2, ?3, ?4 );", nativeQuery = true)
    int saveMessage(Long classId, LocalDateTime time, String content, String sender);

    @Query(value = "SELECT COUNT(m) FROM Message m WHERE m.senderId = ?1")
    long countMessageBySenderId(String studentId);
}
