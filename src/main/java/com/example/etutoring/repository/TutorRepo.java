package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Tutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TutorRepo extends TutorRepoCustom, JpaRepository<Tutor, String> {

    Tutor findByUsername(String username);
}
