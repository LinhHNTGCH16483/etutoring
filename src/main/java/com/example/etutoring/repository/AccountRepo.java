package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface AccountRepo extends JpaRepository<Account, String> {
    Account findAccountByUsernameAndPassword(String username, String password);

    @Modifying
    @Query(value = "UPDATE account a SET a.last_login = ?1 WHERE a.username = ?2 ;", nativeQuery = true)
    int updateAccount(LocalDateTime lastLogin, String username);
}
