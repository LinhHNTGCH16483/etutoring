package com.example.etutoring.repository;

import com.example.etutoring.domain.dto.FileDto;

import java.util.List;

public interface FileRepoCustom {

    List<FileDto> getFileByClass(Long classId);
}
