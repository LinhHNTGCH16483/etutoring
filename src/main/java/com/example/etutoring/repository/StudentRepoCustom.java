package com.example.etutoring.repository;

import java.util.List;

public interface StudentRepoCustom {
    List<?> getAllStudentInfor();

    List<?> getStudentNoTutor();

    List<?> getStudentNoInteraction(Long day);
}
