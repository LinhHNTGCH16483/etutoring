package com.example.etutoring.repository;

import com.example.etutoring.domain.dto.ClassInforDto;

import java.util.List;

public interface ClassRepoCustom {
    List<?> getAllClassInfo();

    List<ClassInforDto> getClassByStudent(String studentId);

    List<?> getClassByTutor(String tutorId);
}
