package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface FileRepo extends FileRepoCustom, JpaRepository<File, Long> {

    @Modifying
    @Query(value = "INSERT INTO file( class_id, uri, name, type, size , time) VALUES ( ?1, ?2, ?3, ?4, ?5, ?6 );", nativeQuery = true)
    int insertFile(Long classId, String uri, String name, String type, Long size, LocalDateTime time);
}
