package com.example.etutoring.repository.Impl;

import com.example.etutoring.repository.StaffRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class StaffRepoCustomImpl implements StaffRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<?> getAllStaffInfor() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        s.staff_id, ");
        stringQuery.append("        s.staff_name, ");
        stringQuery.append("        s.phone_number, ");
        stringQuery.append("        s.email, ");
        stringQuery.append("        s.address, ");
        stringQuery.append("        s.date_of_birth, ");
        stringQuery.append("        s.avatar, ");
        stringQuery.append("        a.username, ");
        stringQuery.append("        a.last_login ");
        stringQuery.append("FROM    staff s ");
        stringQuery.append("JOIN    account a ON s.username = a.username;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return query.getResultList();
    }
}
