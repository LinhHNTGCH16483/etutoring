package com.example.etutoring.repository.Impl;

import com.example.etutoring.domain.dto.ClassInforDto;
import com.example.etutoring.repository.ClassRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

import static com.example.etutoring.common.DataUtils.parseToLong;
import static com.example.etutoring.common.DataUtils.parseToString;

public class ClassRepoCustomImpl implements ClassRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<?> getAllClassInfo() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT c.class_id, s.student_id, s.student_name, t.tutor_id, t.tutor_name ");
        stringQuery.append("FROM class c ");
        stringQuery.append("JOIN student s ON c.student_id = s.student_id ");
        stringQuery.append("JOIN tutor t ON c.tutor_id = t.tutor_id;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return query.getResultList();
    }

    @Override
    public List<ClassInforDto> getClassByStudent(String studentId) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT c.class_id, s.student_id, s.student_name, t.tutor_id, t.tutor_name ");
        stringQuery.append("FROM class c ");
        stringQuery.append("JOIN student s ON c.student_id = s.student_id ");
        stringQuery.append("JOIN tutor t ON c.tutor_id = t.tutor_id ");
        stringQuery.append("WHERE s.student_id = :studentId ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("studentId", studentId);
        return this.parseToListClassInforDto(query.getResultList());
    }

    @Override
    public List<?> getClassByTutor(String tutorId) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT c.class_id, s.student_id, s.student_name, t.tutor_id, t.tutor_name ");
        stringQuery.append("FROM class c ");
        stringQuery.append("JOIN student s ON c.student_id = s.student_id ");
        stringQuery.append("JOIN tutor t ON c.tutor_id = t.tutor_id ");
        stringQuery.append("WHERE t.tutor_id = :tutorId ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("tutorId", tutorId);
        return query.getResultList();
    }

    private List<ClassInforDto> parseToListClassInforDto(List<Object[]> objects) {
        List<ClassInforDto> result = new ArrayList<>();
        for (Object[] obj : objects) {
            int i = 0;
            result.add(ClassInforDto.builder()
                    .classId(parseToLong(obj[i++]))
                    .studentId(parseToString(obj[i++]))
                    .studentName(parseToString(obj[i++]))
                    .tutorId(parseToString(obj[i++]))
                    .tutorName(parseToString(obj[i++]))
                    .build());
        }
        return result;
    }
}
