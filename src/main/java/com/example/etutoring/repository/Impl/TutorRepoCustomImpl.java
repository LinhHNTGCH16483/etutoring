package com.example.etutoring.repository.Impl;

import com.example.etutoring.repository.TutorRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class TutorRepoCustomImpl implements TutorRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<?> getAllTutorInfor() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        t.tutor_id, ");
        stringQuery.append("        t.tutor_name, ");
        stringQuery.append("        t.phone_number, ");
        stringQuery.append("        t.email, ");
        stringQuery.append("        t.address, ");
        stringQuery.append("        t.date_of_birth, ");
        stringQuery.append("        t.avatar, ");
        stringQuery.append("        a.username, ");
        stringQuery.append("        a.last_login ");
        stringQuery.append("FROM    tutor t ");
        stringQuery.append("JOIN    account a ON t.username = a.username;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return query.getResultList();
    }
}
