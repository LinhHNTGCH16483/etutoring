package com.example.etutoring.repository.Impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectUtils;
import com.example.etutoring.domain.dto.FileDto;
import com.example.etutoring.repository.FileRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class FileRepoCustomImpl implements FileRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<FileDto> getFileByClass(Long classId) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        f.* ");
        stringQuery.append("FROM    file f ");
        stringQuery.append("WHERE   f.class_id = :classId ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("classId", classId);
        return this.parseToFileDtoList(query.getResultList());
    }

    private List<FileDto> parseToFileDtoList(List<Object[]> result) {
        if (ObjectUtils.notNull(result)) {
            List<FileDto> fileDtoList = new ArrayList<>();
            for (Object[] objects : result) {
                FileDto fileDto = FileDto.builder()
                        .fileId(DataUtils.parseToLong(objects[0]))
                        .classId(DataUtils.parseToLong(objects[1]))
                        .uri(DataUtils.parseToString(objects[2]))
                        .name(DataUtils.parseToString(objects[3]))
                        .type(DataUtils.parseToString(objects[4]))
                        .size(DataUtils.parseToLong(objects[5]))
                        .time(DataUtils.parseToLocalDateTime(objects[6]))
                        .build();
                fileDtoList.add(fileDto);
            }
            return fileDtoList;
        }
        return new ArrayList<>();
    }
}
