package com.example.etutoring.repository.Impl;

import com.example.etutoring.repository.StudentRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

public class StudentRepoCustomImpl implements StudentRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<?> getAllStudentInfor() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        s.student_id, ");
        stringQuery.append("        s.student_name, ");
        stringQuery.append("        s.phone_number, ");
        stringQuery.append("        s.email, ");
        stringQuery.append("        s.address, ");
        stringQuery.append("        s.date_of_birth, ");
        stringQuery.append("        s.avatar, ");
        stringQuery.append("        a.username, ");
        stringQuery.append("        a.last_login ");
        stringQuery.append("FROM    student s ");
        stringQuery.append("JOIN    account a ON s.username = a.username;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return query.getResultList();
    }

    @Override
    public List<?> getStudentNoTutor() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        s.student_id, ");
        stringQuery.append("        s.student_name, ");
        stringQuery.append("        s.phone_number, ");
        stringQuery.append("        s.email, ");
        stringQuery.append("        s.address, ");
        stringQuery.append("        s.date_of_birth, ");
        stringQuery.append("        s.avatar, ");
        stringQuery.append("        a.username, ");
        stringQuery.append("        a.last_login ");
        stringQuery.append("FROM    student s ");
        stringQuery.append("JOIN    account a ON s.username = a.username ");
        stringQuery.append("WHERE   s.student_id NOT IN ");
        stringQuery.append("                           (SELECT c.student_id ");
        stringQuery.append("                            FROM class c);");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return query.getResultList();
    }

    @Override
    public List<?> getStudentNoInteraction(Long day) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        s.student_id, ");
        stringQuery.append("        s.student_name, ");
        stringQuery.append("        s.phone_number, ");
        stringQuery.append("        s.email, ");
        stringQuery.append("        s.address, ");
        stringQuery.append("        s.date_of_birth, ");
        stringQuery.append("        s.avatar, ");
        stringQuery.append("        a.username, ");
        stringQuery.append("        a.last_login ");
        stringQuery.append("FROM    student s ");
        stringQuery.append("JOIN    account a ON s.username = a.username ");
        stringQuery.append("WHERE   a.last_Login <= :time ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("time", LocalDateTime.now().minusDays(day));
        return query.getResultList();
    }
}
