package com.example.etutoring.repository.Impl;

import com.example.etutoring.domain.dto.MessageReportDto;
import com.example.etutoring.repository.MessageRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.etutoring.common.DataUtils.parseToLong;
import static com.example.etutoring.common.DataUtils.parseToString;

public class MessageRepoCustomImpl implements MessageRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<MessageReportDto> getStudentMessage() {
        StringBuilder stringQuery = new StringBuilder()
                .append("SELECT     s.student_id, s.student_name, t.tutor_name, COUNT(s.student_id) ")
                .append("FROM       message m ")
                .append("JOIN student s ON m.sender = s.student_id ")
                .append("JOIN class c ON s.student_id = c.student_id ")
                .append("JOIN tutor t ON c.tutor_id = t.tutor_id ")
                .append("GROUP BY s.student_id;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        return parseToMessageReportDtoList(query.getResultList());
    }

    private List<MessageReportDto> parseToMessageReportDtoList(List<Object[]> objects) {
        List<MessageReportDto> messageReportDtoList = new ArrayList<>();
        for (Object[] obj : objects) {
            int i = 0;
            messageReportDtoList.add(MessageReportDto.builder()
                    .studentId(parseToString(obj[i++]))
                    .studentName(parseToString(obj[i++]))
                    .tutorName(parseToString(obj[i++]))
                    .totalMessage(parseToLong(obj[i++]))
                    .build());
        }
        return messageReportDtoList;
    }

    @Override
    public List<?> getMessageIn7Day() {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        m.* ");
        stringQuery.append("FROM    message m ");
        stringQuery.append("WHERE   m.time >= :time ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("time", LocalDateTime.now().minusDays(7));
        return query.getResultList();
    }

    @Override
    public List<?> getMessageByClass(Long classId) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        m.* ");
        stringQuery.append("FROM    message m ");
        stringQuery.append("WHERE   m.class_id = :classId ;");
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("classId", classId);
        return query.getResultList();
    }
}
