package com.example.etutoring.repository.Impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectUtils;
import com.example.etutoring.domain.dto.ScheduleDto;
import com.example.etutoring.repository.ScheduleRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ScheduleRepoCustomImpl implements ScheduleRepoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<ScheduleDto> getPersonSchedule(String studentId, String tutorId, LocalDate fromDate, LocalDate toDate) {
        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append("SELECT ");
        stringQuery.append("        s.schedule_id, ");
        if (ObjectUtils.isNull(studentId)) {
            stringQuery.append("    c.student_id, ");
        } else {
            stringQuery.append("    c.tutor_id, ");
        }
        stringQuery.append("        s.date, ");
        stringQuery.append("        s.slot_1, ");
        stringQuery.append("        s.slot_2, ");
        stringQuery.append("        s.slot_3, ");
        stringQuery.append("        s.slot_4, ");
        stringQuery.append("        s.slot_5, ");
        stringQuery.append("        s.slot_6, ");
        stringQuery.append("        s.slot_7, ");
        stringQuery.append("        s.slot_8 ");
        stringQuery.append("FROM    schedule s ");
        stringQuery.append("JOIN    class c ON s.class_id = c.class_id ");
        stringQuery.append("WHERE   (s.date BETWEEN :fromDate AND :toDate) ");
        if (ObjectUtils.isNull(tutorId)) {
            stringQuery.append("AND   c.student_id = :studentId ;");
        } else {
            stringQuery.append("AND   c.tutor_id = :tutorId ;");
        }
        Query query = entityManager.createNativeQuery(stringQuery.toString());
        query.setParameter("fromDate", fromDate.plusDays(1));
        query.setParameter("toDate", toDate.plusDays(1));
        if (ObjectUtils.isNull(tutorId)) {
            query.setParameter("studentId", studentId);
        } else {
            query.setParameter("tutorId", tutorId);
        }
        return this.parseToScheduleDtoList(query.getResultList());
    }

    private List<ScheduleDto> parseToScheduleDtoList(List<Object[]> result) {
        if (ObjectUtils.notNull(result)) {
            List<ScheduleDto> scheduleDtoList = new ArrayList<>();
            for (Object[] objects : result) {
                ScheduleDto scheduleDto = ScheduleDto.builder()
                        .scheduleId(DataUtils.parseToLong(objects[0]))
                        .id(DataUtils.parseToString(objects[1]))
                        .date(DataUtils.parseToLocalDate(objects[2]))
                        .slot1(DataUtils.parseToBoolean(objects[3]))
                        .slot2(DataUtils.parseToBoolean(objects[4]))
                        .slot3(DataUtils.parseToBoolean(objects[5]))
                        .slot4(DataUtils.parseToBoolean(objects[6]))
                        .slot5(DataUtils.parseToBoolean(objects[7]))
                        .slot6(DataUtils.parseToBoolean(objects[8]))
                        .slot7(DataUtils.parseToBoolean(objects[9]))
                        .slot8(DataUtils.parseToBoolean(objects[10]))
                        .build();
                scheduleDtoList.add(scheduleDto);
            }
            return scheduleDtoList;
        }
        return new ArrayList<>();
    }
}
