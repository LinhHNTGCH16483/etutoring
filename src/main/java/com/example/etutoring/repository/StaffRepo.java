package com.example.etutoring.repository;

import com.example.etutoring.domain.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepo extends StaffRepoCustom, JpaRepository<Staff, String> {

    Staff findByUsername(String username);
}
