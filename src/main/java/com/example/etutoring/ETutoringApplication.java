package com.example.etutoring;

import com.example.etutoring.config.FileStorageConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageConfig.class
})
public class ETutoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ETutoringApplication.class, args);
    }

}
