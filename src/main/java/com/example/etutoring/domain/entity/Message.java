package com.example.etutoring.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "message")
public class Message {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "message_id")
    private Long id;

    @Column(name = "class_id")
    private Long classId;

    @Column(name = "time")
    private LocalDateTime time;

    @Column(name = "content")
    private String content;

    @Column(name = "sender")
    private String senderId;
}
