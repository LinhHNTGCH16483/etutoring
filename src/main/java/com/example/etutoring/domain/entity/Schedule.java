package com.example.etutoring.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "schedule")
public class Schedule {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "schedule_id")
    private Long id;

    @Column(name = "class_id")
    private Long classId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "slot_1")
    private Boolean slot1;

    @Column(name = "slot_2")
    private Boolean slot2;

    @Column(name = "slot_3")
    private Boolean slot3;

    @Column(name = "slot_4")
    private Boolean slot4;

    @Column(name = "slot_5")
    private Boolean slot5;

    @Column(name = "slot_6")
    private Boolean slot6;

    @Column(name = "slot_7")
    private Boolean slot7;

    @Column(name = "slot_8")
    private Boolean slot8;
}
