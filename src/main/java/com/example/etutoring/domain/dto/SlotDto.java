package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class SlotDto implements Serializable {

    private String id;
    private Boolean slot;
}
