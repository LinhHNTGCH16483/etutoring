package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class AccountDto implements Serializable {

    private String username;
    private String password;
    private String type;
    private LocalDateTime lastLogin;
}
