package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
public class ScheduleDto implements Serializable {

    private Long scheduleId;
    private String id;
    private LocalDate date;
    private Boolean slot1;
    private Boolean slot2;
    private Boolean slot3;
    private Boolean slot4;
    private Boolean slot5;
    private Boolean slot6;
    private Boolean slot7;
    private Boolean slot8;
}
