package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
public class TutorDto implements Serializable {

    private String id;
    private String name;
    private String phoneNumber;
    private String email;
    private String address;
    private LocalDate dateOfBirth;
    private String avatar;
    private String username;
}
