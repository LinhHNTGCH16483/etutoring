package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class TutorAccountDto implements Serializable {

    private String id;
    private String name;
    private String phoneNumber;
    private String email;
    private String address;
    private LocalDate dateOfBirth;
    private String avatar;
    private String username;
    private LocalDateTime lastLogin;
}
