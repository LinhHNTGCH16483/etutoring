package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class FileResponeDto implements Serializable {

    private String uri;
    private String name;
    private String type;
    private Long size;
    private LocalDateTime time;
}
