package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
public class CalendarDto implements Serializable {

    private LocalDate date;
    private SlotDto slot1;
    private SlotDto slot2;
    private SlotDto slot3;
    private SlotDto slot4;
    private SlotDto slot5;
    private SlotDto slot6;
    private SlotDto slot7;
    private SlotDto slot8;
}
