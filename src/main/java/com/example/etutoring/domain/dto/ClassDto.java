package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ClassDto implements Serializable {

    private Long classId;
    private String studentId;
    private String tutorId;
}
