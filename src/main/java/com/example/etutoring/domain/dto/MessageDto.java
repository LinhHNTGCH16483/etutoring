package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class MessageDto implements Serializable {

    private Long messageId;
    private Long classId;
    private LocalDateTime time;
    private String content;
    private String sender;
}
