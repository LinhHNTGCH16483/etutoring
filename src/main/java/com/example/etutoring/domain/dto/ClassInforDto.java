package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ClassInforDto implements Serializable {

    private Long classId;
    private String studentId;
    private String studentName;
    private String tutorId;
    private String tutorName;
}
