package com.example.etutoring.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class MessageReportDto implements Serializable {

    private String studentId;
    private String studentName;
    private String tutorName;
    private Long totalMessage;
}
