package com.example.etutoring.service;

import com.example.etutoring.domain.dto.MessageDto;
import com.example.etutoring.domain.dto.MessageReportDto;

import java.util.List;

public interface MessageService {

    Boolean saveMessage(MessageDto messageDto);

    List<MessageReportDto> getStudentMessage();

    Long countMessageIn7Day();

    Long averageTutorMessage();

    Long averageStudentMessage();

    List<MessageDto> getMessageByClass(Long classId);

    Long countStudentMessage(String studentId);

    Long countTutorMessage(String tutorId);
}
