package com.example.etutoring.service;

import com.example.etutoring.domain.dto.AccountDto;
import com.example.etutoring.domain.dto.LoginDto;

public interface AccountService {

    AccountDto findAccountByUsernameAndPassword(LoginDto loginDto);
}
