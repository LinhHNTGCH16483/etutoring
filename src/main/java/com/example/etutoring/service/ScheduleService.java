package com.example.etutoring.service;

import com.example.etutoring.domain.dto.CalendarDto;
import com.example.etutoring.domain.dto.ScheduleDto;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleService {

    Boolean createMeeting(ScheduleDto scheduleDto);

    List<CalendarDto> getPersonSchedule(String studentId, String tutorId, LocalDate fromDate, LocalDate toDate);
}
