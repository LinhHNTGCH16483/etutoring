package com.example.etutoring.service;

import com.example.etutoring.domain.dto.StaffAccountDto;
import com.example.etutoring.domain.dto.StaffDto;

import java.util.List;

public interface StaffService {

    List<StaffAccountDto> getAllStaffInfor();

    StaffDto getStaffByUsername(String username);

    StaffDto getStaffById(String staffId);

}
