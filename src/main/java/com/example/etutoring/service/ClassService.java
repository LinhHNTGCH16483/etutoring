package com.example.etutoring.service;

import com.example.etutoring.domain.dto.ClassDto;
import com.example.etutoring.domain.dto.ClassInforDto;

import java.util.List;

public interface ClassService {

    List<ClassInforDto> getAllClassInfor();

    ClassInforDto getClassByStudent(String studentId);

    List<ClassInforDto> getClassByTutor(String tutorId);

    ClassDto getClassById(Long classId);

    boolean addNewClass(ClassDto classDto);

    boolean addClasses(List<ClassDto> classDtoList);

    boolean updateClass(ClassDto classDto);
}
