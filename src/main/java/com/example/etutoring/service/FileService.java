package com.example.etutoring.service;

import com.example.etutoring.domain.dto.FileDto;
import com.example.etutoring.domain.dto.FileResponeDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FileService {

    FileResponeDto storeFile(Long classId, MultipartFile file);

    List<FileResponeDto> storeFiles(Long classId, MultipartFile[] files);

    ResponseEntity<Resource> loadFile(String fileName, HttpServletRequest httpServletRequest);

    List<FileDto> getFileByClass(Long classId);
}
