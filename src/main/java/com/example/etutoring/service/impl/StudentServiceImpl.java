package com.example.etutoring.service.impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectMapper;
import com.example.etutoring.domain.dto.StudentAccountDto;
import com.example.etutoring.domain.dto.StudentDto;
import com.example.etutoring.repository.StudentRepo;
import com.example.etutoring.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepo studentRepo;

    @Override
    public List<StudentAccountDto> getAllStudentInfor() {
        return (List<StudentAccountDto>) studentRepo.getAllStudentInfor();
    }

    @Override
    public StudentDto getStudentById(String studentId) {
        return ObjectMapper.mapToStudentDto(studentRepo.findById(studentId).orElse(null));
    }

    @Override
    public StudentDto getStudentByUsername(String username) {
        return ObjectMapper.mapToStudentDto(studentRepo.findByUsername(username));
    }

    @Override
    public Long getNumberOfStudent() {
        return studentRepo.count();
    }

    @Override
    public List<StudentAccountDto> getStudentNoTutor() {
        return (List<StudentAccountDto>) studentRepo.getStudentNoTutor();
    }

    @Override
    public Long getNumberOfStudentNoTutor() {
        return DataUtils.parseToLong(studentRepo.getStudentNoTutor().size());
    }

    @Override
    public List<StudentAccountDto> getStudentNoInteraction(Long day) {
        return (List<StudentAccountDto>) studentRepo.getStudentNoInteraction(day);
    }
}
