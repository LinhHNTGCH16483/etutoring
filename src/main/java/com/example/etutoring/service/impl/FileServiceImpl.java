package com.example.etutoring.service.impl;

import com.example.etutoring.common.Exception.FileNotFoundException;
import com.example.etutoring.common.Exception.FileStorageException;
import com.example.etutoring.config.FileStorageConfig;
import com.example.etutoring.domain.dto.FileDto;
import com.example.etutoring.domain.dto.FileResponeDto;
import com.example.etutoring.repository.FileRepo;
import com.example.etutoring.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FileServiceImpl implements FileService {

    private final Path fileStorageLocation;

    @Autowired
    FileRepo fileRepo;

    @Autowired
    public FileServiceImpl(FileStorageConfig fileStorageConfig) {
        this.fileStorageLocation = Paths.get(fileStorageConfig.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Fail to create directory to store the uploaded files", ex);
        }
    }

    @Override
    public FileResponeDto storeFile(Long classId, MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files?fileName=" + fileName)
                    .toUriString().replaceFirst("%3F", "?");
            fileRepo.insertFile(classId, uri, fileName, file.getContentType(), file.getSize(), LocalDateTime.now());
            return FileResponeDto.builder()
                    .uri(uri)
                    .name(fileName)
                    .type(file.getContentType())
                    .size(file.getSize())
                    .time(LocalDateTime.now())
                    .build();
        } catch (IOException ex) {
            throw new FileStorageException("Fail to store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public List<FileResponeDto> storeFiles(Long classId, MultipartFile[] files) {
        List<FileResponeDto> fileResponeDtoList = new ArrayList<>();
        for (MultipartFile file : files) {
            fileResponeDtoList.add(storeFile(classId, file));
        }
        return fileResponeDtoList;
    }

    @Override
    public ResponseEntity<Resource> loadFile(String fileName, HttpServletRequest httpServletRequest) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                String contentType = httpServletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
                if (contentType == null) {
                    contentType = "application/octet-stream";
                }
                return ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(contentType))
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName, ex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @Override
    public List<FileDto> getFileByClass(Long classId) {
        return fileRepo.getFileByClass(classId);
    }
}
