package com.example.etutoring.service.impl;

import com.example.etutoring.common.ObjectMapper;
import com.example.etutoring.domain.dto.StaffAccountDto;
import com.example.etutoring.domain.dto.StaffDto;
import com.example.etutoring.repository.StaffRepo;
import com.example.etutoring.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StaffServiceImpl implements StaffService {

    @Autowired
    StaffRepo staffRepo;

    @Override
    public List<StaffAccountDto> getAllStaffInfor() {
        return (List<StaffAccountDto>) staffRepo.getAllStaffInfor();
    }

    @Override
    public StaffDto getStaffByUsername(String username) {
        return ObjectMapper.mapToStaffDto(staffRepo.findByUsername(username));
    }

    @Override
    public StaffDto getStaffById(String staffId) {
        return ObjectMapper.mapToStaffDto(staffRepo.findById(staffId).orElse(null));
    }
}
