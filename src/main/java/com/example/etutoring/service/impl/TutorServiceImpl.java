package com.example.etutoring.service.impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectMapper;
import com.example.etutoring.domain.dto.TutorAccountDto;
import com.example.etutoring.domain.dto.TutorDto;
import com.example.etutoring.repository.ClassRepo;
import com.example.etutoring.repository.TutorRepo;
import com.example.etutoring.service.TutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TutorServiceImpl implements TutorService {

    @Autowired
    TutorRepo tutorRepo;

    @Autowired
    ClassRepo classRepo;

    @Override
    public List<TutorAccountDto> getAllTutorInfor() {
        return (List<TutorAccountDto>) tutorRepo.getAllTutorInfor();
    }

    @Override
    public TutorDto getTutorById(String tutorId) {
        return ObjectMapper.mapToTutorDto(tutorRepo.findById(tutorId).orElse(null));
    }

    @Override
    public TutorDto getTutorByUsername(String username) {
        return ObjectMapper.mapToTutorDto(tutorRepo.findByUsername(username));
    }

    @Override
    public Long getNumberOfTutor() {
        return tutorRepo.count();
    }

    @Override
    public Long getStudentNumber(String tutorId) {
        return DataUtils.parseToLong(classRepo.getClassByTutor(tutorId).size());
    }
}
