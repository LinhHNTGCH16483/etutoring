package com.example.etutoring.service.impl;

import com.example.etutoring.common.Const;
import com.example.etutoring.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    JavaMailSender javaMailSender;

    @Override
    public void sendEmail(String receiver, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(receiver);
        message.setSubject(Const.Email.SUBJECT);
        message.setText(content);
        javaMailSender.send(message);
    }
}
