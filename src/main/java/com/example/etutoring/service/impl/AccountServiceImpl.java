package com.example.etutoring.service.impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectMapper;
import com.example.etutoring.common.ObjectUtils;
import com.example.etutoring.domain.dto.AccountDto;
import com.example.etutoring.domain.dto.LoginDto;
import com.example.etutoring.domain.entity.Account;
import com.example.etutoring.repository.AccountRepo;
import com.example.etutoring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public AccountDto findAccountByUsernameAndPassword(LoginDto loginDto) {
        if (ObjectUtils.isNull(loginDto)) {
            return null;
        }
        String encryptedPassword = DataUtils.encrypt(loginDto.getPassword());
        Account account = accountRepo.findAccountByUsernameAndPassword(loginDto.getUsername(), encryptedPassword);
        if (ObjectUtils.notNull(account)) {
            accountRepo.updateAccount(LocalDateTime.now(), account.getUsername());
        }
        return ObjectMapper.mapToAccountDto(account);
    }
}
