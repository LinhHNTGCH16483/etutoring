package com.example.etutoring.service.impl;

import com.example.etutoring.common.ObjectMapper;
import com.example.etutoring.common.ObjectUtils;
import com.example.etutoring.domain.dto.ClassDto;
import com.example.etutoring.domain.dto.ClassInforDto;
import com.example.etutoring.domain.dto.StudentDto;
import com.example.etutoring.domain.dto.TutorDto;
import com.example.etutoring.repository.ClassRepo;
import com.example.etutoring.repository.StudentRepo;
import com.example.etutoring.repository.TutorRepo;
import com.example.etutoring.service.ClassService;
import com.example.etutoring.service.EmailService;
import com.example.etutoring.service.StudentService;
import com.example.etutoring.service.TutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClassServiceImpl implements ClassService {

    @Autowired
    ClassRepo classRepo;

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    TutorRepo tutorRepo;

    @Autowired
    EmailService emailService;

    @Autowired
    StudentService studentService;

    @Autowired
    TutorService tutorService;

    @Override
    public List<ClassInforDto> getAllClassInfor() {
        return (List<ClassInforDto>) classRepo.getAllClassInfo();
    }

    @Override
    public ClassInforDto getClassByStudent(String studentId) {
        List<ClassInforDto> dtoList = classRepo.getClassByStudent(studentId);
        return (dtoList == null || dtoList.isEmpty()) ? null : dtoList.get(0);
    }

    @Override
    public List<ClassInforDto> getClassByTutor(String tutorId) {
        return (List<ClassInforDto>) classRepo.getClassByTutor(tutorId);
    }

    @Override
    public ClassDto getClassById(Long classId) {
        return ObjectMapper.mapToClassDto(classRepo.findById(classId).orElse(null));
    }

    @Override
    public boolean addNewClass(ClassDto classDto) {
        if (ObjectUtils.isNull(classDto)) {
            return false;
        } else if (!studentRepo.existsById(classDto.getStudentId())) {
            return false;
        } else if (!tutorRepo.existsById(classDto.getTutorId())) {
            return false;
        }
        int result = classRepo.insertClass(classDto.getStudentId(), classDto.getTutorId());
        if (result == 1) {
            StudentDto studentDto = studentService.getStudentById(classDto.getStudentId());
            TutorDto tutorDto = tutorService.getTutorById(classDto.getTutorId());

            StringBuilder studentMsg = new StringBuilder();
            studentMsg.append("Dear " + studentDto.getName() + ",\n");
            studentMsg.append("Your personal tutor is " + tutorDto.getName() + ".");
            emailService.sendEmail(studentDto.getEmail(), studentMsg.toString());

            StringBuilder tutorMsg = new StringBuilder();
            tutorMsg.append("Dear " + tutorDto.getName() + ",\n");
            tutorMsg.append("You have became personal tutor of " + studentDto.getName() + ".");
            emailService.sendEmail(tutorDto.getEmail(), tutorMsg.toString());
        }
        return result == 1;
    }

    @Override
    public boolean addClasses(List<ClassDto> classDtoList) {
        for (ClassDto dto : classDtoList) {
            boolean flag = addNewClass(dto);
            if (!flag) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean updateClass(ClassDto classDto) {
        if (ObjectUtils.isNull(classDto)) {
            return false;
        } else if (!studentRepo.existsById(classDto.getStudentId())) {
            return false;
        } else if (!tutorRepo.existsById(classDto.getTutorId())) {
            return false;
        }
        int result = classRepo.updateClass(classDto.getStudentId(), classDto.getTutorId(), classDto.getClassId());
        if (result == 1) {
            StudentDto studentDto = studentService.getStudentById(classDto.getStudentId());
            TutorDto tutorDto = tutorService.getTutorById(classDto.getTutorId());

            StringBuilder studentMsg = new StringBuilder();
            studentMsg.append("Dear " + studentDto.getName() + ",\n");
            studentMsg.append("Your personal tutor has change to: " + tutorDto.getName());
            emailService.sendEmail(studentDto.getEmail(), studentMsg.toString());

            StringBuilder tutorMsg = new StringBuilder();
            tutorMsg.append("Dear " + tutorDto.getName() + ",\n");
            tutorMsg.append("You have became personal tutor of: " + studentDto.getName());
            emailService.sendEmail(tutorDto.getEmail(), tutorMsg.toString());
        }
        return result == 1;
    }
}
