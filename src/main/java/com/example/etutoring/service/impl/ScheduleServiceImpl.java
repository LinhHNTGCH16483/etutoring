package com.example.etutoring.service.impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.common.ObjectUtils;
import com.example.etutoring.domain.dto.*;
import com.example.etutoring.repository.ScheduleRepo;
import com.example.etutoring.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    ScheduleRepo scheduleRepo;

    @Autowired
    ClassService classService;

    @Autowired
    StudentService studentService;

    @Autowired
    TutorService tutorService;

    @Autowired
    EmailService emailService;

    @Override
    public Boolean createMeeting(ScheduleDto scheduleDto) {
        if (ObjectUtils.isNull(scheduleDto)) {
            return false;
        }
        int row = scheduleRepo.createMeeting(DataUtils.parseToLong(scheduleDto.getId()), scheduleDto.getDate().plusDays(1),
                scheduleDto.getSlot1(), scheduleDto.getSlot2(), scheduleDto.getSlot3(), scheduleDto.getSlot4(),
                scheduleDto.getSlot5(), scheduleDto.getSlot6(), scheduleDto.getSlot7(), scheduleDto.getSlot8());
        if (row == 1) {
            ClassDto classDto = classService.getClassById(DataUtils.parseToLong(scheduleDto.getId()));
            StudentDto studentDto = studentService.getStudentById(classDto.getStudentId());
            TutorDto tutorDto = tutorService.getTutorById(classDto.getTutorId());

            StringBuilder studentMsg = new StringBuilder();
            studentMsg.append("Dear " + studentDto.getName() + ",\n");
            studentMsg.append("You have an online meeting with " + tutorDto.getName() + ".");
            emailService.sendEmail(studentDto.getEmail(), studentMsg.toString());

            StringBuilder tutorMsg = new StringBuilder();
            tutorMsg.append("Dear " + tutorDto.getName() + ",\n");
            tutorMsg.append("You just create an online meeting with " + studentDto.getName() + ".");
            emailService.sendEmail(tutorDto.getEmail(), tutorMsg.toString());
        }
        return row == 1;
    }

    @Override
    public List<CalendarDto> getPersonSchedule(String studentId, String tutorId, LocalDate fromDate, LocalDate toDate) {
        if (ObjectUtils.isNull(studentId) && ObjectUtils.isNull(tutorId)) {
            return new ArrayList<>();
        }
        List<ScheduleDto> scheduleDtoList = scheduleRepo.getPersonSchedule(studentId, tutorId, fromDate, toDate);
        List<CalendarDto> calendarDtoList = new ArrayList<>();
        scheduleDtoList.sort(Comparator.comparing(ScheduleDto::getDate));
        for (int i = 0; i < 7; i++) {
            calendarDtoList.add(CalendarDto.builder()
                    .date(fromDate.plusDays(i))
                    .slot1(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot2(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot3(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot4(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot5(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot6(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot7(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .slot8(SlotDto.builder()
                            .id(null)
                            .slot(false)
                            .build())
                    .build());
            for (ScheduleDto scheduleDto : scheduleDtoList) {
                if (scheduleDto.getDate().equals(fromDate.plusDays(i))) {
                    calendarDtoList.set(i, DataUtils.mergeSchedule(calendarDtoList.get(i), scheduleDto));
                } else {
                    break;
                }
            }
            int finalI = i;
            scheduleDtoList.removeIf(item -> item.getDate().equals(fromDate.plusDays(finalI)));
        }
        return calendarDtoList;
    }
}
