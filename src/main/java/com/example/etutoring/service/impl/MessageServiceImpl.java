package com.example.etutoring.service.impl;

import com.example.etutoring.common.DataUtils;
import com.example.etutoring.domain.dto.MessageDto;
import com.example.etutoring.domain.dto.MessageReportDto;
import com.example.etutoring.repository.MessageRepo;
import com.example.etutoring.repository.StudentRepo;
import com.example.etutoring.repository.TutorRepo;
import com.example.etutoring.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageRepo messageRepo;

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    TutorRepo tutorRepo;

    @Override
    public Boolean saveMessage(MessageDto messageDto) {
        int row = messageRepo.saveMessage(messageDto.getClassId(), LocalDateTime.now(), messageDto.getContent(), messageDto.getSender());
        return row == 1;
    }

    @Override
    public List<MessageReportDto> getStudentMessage() {
        return messageRepo.getStudentMessage();
    }

    @Override
    public Long countMessageIn7Day() {
        return DataUtils.parseToLong(messageRepo.getMessageIn7Day().size());
    }

    @Override
    public Long averageTutorMessage() {
        return messageRepo.count() / tutorRepo.count();
    }

    @Override
    public Long averageStudentMessage() {
        return messageRepo.count() / studentRepo.count();
    }

    @Override
    public List<MessageDto> getMessageByClass(Long classId) {
        return (List<MessageDto>) messageRepo.getMessageByClass(classId);
    }

    @Override
    public Long countStudentMessage(String studentId) {
        return messageRepo.countMessageBySenderId(studentId);
    }

    @Override
    public Long countTutorMessage(String tutorId) {
        return messageRepo.countMessageBySenderId(tutorId);
    }
}
