package com.example.etutoring.service;

import com.example.etutoring.domain.dto.TutorAccountDto;
import com.example.etutoring.domain.dto.TutorDto;

import java.util.List;

public interface TutorService {

    List<TutorAccountDto> getAllTutorInfor();

    TutorDto getTutorById(String tutorId);

    TutorDto getTutorByUsername(String username);

    Long getNumberOfTutor();

    Long getStudentNumber(String tutorId);

}
