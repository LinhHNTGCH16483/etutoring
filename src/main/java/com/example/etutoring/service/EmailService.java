package com.example.etutoring.service;

public interface EmailService {

    void sendEmail(String receiver, String content);
}
