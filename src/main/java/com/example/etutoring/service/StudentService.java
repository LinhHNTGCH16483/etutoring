package com.example.etutoring.service;

import com.example.etutoring.domain.dto.StudentAccountDto;
import com.example.etutoring.domain.dto.StudentDto;

import java.util.List;

public interface StudentService {

    List<StudentAccountDto> getAllStudentInfor();

    StudentDto getStudentById(String studentId);

    StudentDto getStudentByUsername(String username);

    Long getNumberOfStudent();

    List<StudentAccountDto> getStudentNoTutor();

    Long getNumberOfStudentNoTutor();

    List<StudentAccountDto> getStudentNoInteraction(Long day);
}
