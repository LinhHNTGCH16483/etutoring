-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for e-tutoring
DROP DATABASE IF EXISTS `e-tutoring`;
CREATE DATABASE IF NOT EXISTS `e-tutoring` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `e-tutoring`;

-- Dumping structure for table e-tutoring.account
DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account`
(
    `username`   varchar(50)  NOT NULL,
    `password`   varchar(128) NOT NULL,
    `type`       varchar(10)  NOT NULL,
    `last_login` datetime DEFAULT NULL,
    PRIMARY KEY (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.account: ~50 rows (approximately)
DELETE
FROM `account`;
/*!40000 ALTER TABLE `account`
    DISABLE KEYS */;
INSERT INTO `account` (`username`, `password`, `type`, `last_login`)
VALUES ('BuiThiDung', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-06 14:51:09'),
       ('BuiThiMinhPhuong', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 06:43:26'),
       ('DangThiNgocHuyen', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:14:49'),
       ('DoanTrungTung', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 07:09:26'),
       ('DoHongQuan', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 07:02:14'),
       ('DoQuocBinh', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 07:21:19'),
       ('DoThiTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-03-28 00:00:00'),
       ('DoTienThanh', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:09:52'),
       ('HoangNgocTuanLinh', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-06 14:56:27'),
       ('HoangTheViet', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 17:00:20'),
       ('HoangThiMinh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-05 09:30:02'),
       ('HuaThiThuTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 07:30:52'),
       ('LaiManhDung', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:02:46'),
       ('LaVanNguyen', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:17:46'),
       ('LeHongThu', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 16:58:07'),
       ('NghiemXuanAnh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:10:55'),
       ('NgoThiHangNga', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-04-12 12:57:19'),
       ('NguyenDinhTranLong', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:24:39'),
       ('NguyenHongPhuong', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 07:47:59'),
       ('NguyenMinhHai', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:26:04'),
       ('NguyenMinhHieu', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-04-30 12:00:12'),
       ('NguyenMinhThanh', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-03-28 00:00:00'),
       ('NguyenPhuongDung', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-06 14:56:55'),
       ('NguyenThiAnhAnh', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 16:59:02'),
       ('NguyenThiBichNgoc', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-03-28 00:00:00'),
       ('NguyenThiKhanhLy', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-06 14:55:28'),
       ('NguyenThiLanAnh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-03-28 00:00:00'),
       ('NguyenThiNgan', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:17:29'),
       ('NguyenThiQuynh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:03:03'),
       ('NguyenThiThanhThuy', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-03-28 00:00:00'),
       ('NguyenThiThuy', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 06:48:51'),
       ('NguyenThiTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-03-28 00:00:00'),
       ('NguyenThiTuoiTham', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 07:21:34'),
       ('NguyenTranTuan', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 17:00:38'),
       ('NguyenVanTung', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:15:05'),
       ('NguyenXuanSang', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 16:59:52'),
       ('PhamHoangNam', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 06:58:16'),
       ('PhamHongTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 07:02:30'),
       ('PhamThiNhuQuynh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 07:51:38'),
       ('PhamThuyDuong', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 08:05:14'),
       ('PhamUyenPhuongThao', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-05 09:38:41'),
       ('PhanThiHuyenTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:29:29'),
       ('PhanThiOanh', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:26:18'),
       ('staff', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 09:40:15'),
       ('SuperStaff', 'e10adc3949ba59abbe56e057f20f883e', 'authorized', '2020-05-05 09:44:09'),
       ('TranThiHaiYen', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 07:11:32'),
       ('TruongCongDoan', 'e10adc3949ba59abbe56e057f20f883e', 'tutor', '2020-05-04 07:30:20'),
       ('VuongThuyTrang', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 08:05:36'),
       ('VuThanhHai', 'e10adc3949ba59abbe56e057f20f883e', 'staff', '2020-05-05 16:58:41'),
       ('VuThiThuy', 'e10adc3949ba59abbe56e057f20f883e', 'student', '2020-05-04 14:20:17');
/*!40000 ALTER TABLE `account`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.class
DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class`
(
    `class_id`   bigint(20)  NOT NULL AUTO_INCREMENT,
    `student_id` varchar(20) NOT NULL,
    `tutor_id`   varchar(20) NOT NULL,
    PRIMARY KEY (`class_id`),
    KEY `FK_class_student` (`student_id`),
    KEY `FK_class_tutor` (`tutor_id`),
    CONSTRAINT `FK_class_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
    CONSTRAINT `FK_class_tutor` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`tutor_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 53
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.class: ~19 rows (approximately)
DELETE
FROM `class`;
/*!40000 ALTER TABLE `class`
    DISABLE KEYS */;
INSERT INTO `class` (`class_id`, `student_id`, `tutor_id`)
VALUES (17, 'GBH1692', 'BA002'),
       (33, 'GBH1691', 'BA001'),
       (35, 'GBH1697', 'BA003'),
       (36, 'GBH1709', 'BA003'),
       (37, 'GBH1712', 'IT001'),
       (38, 'GBH1696', 'BA005'),
       (39, 'GBH1703', 'GD001'),
       (40, 'GBH1699', 'IT003'),
       (41, 'GBH1714', 'IT005'),
       (42, 'GBH1700', 'GD004'),
       (43, 'GBH1708', 'IT002'),
       (44, 'GBH1706', 'GD002'),
       (45, 'GBH1701', 'GD003'),
       (46, 'GBH1711', 'GD005'),
       (47, 'GBH1698', 'BA004'),
       (48, 'GBH1702', 'BA001'),
       (49, 'GBH1710', 'IT004'),
       (51, 'GBH1693', 'BA001'),
       (52, 'GBH1694', 'BA001');
/*!40000 ALTER TABLE `class`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.file
DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file`
(
    `file_id`  bigint(20)   NOT NULL AUTO_INCREMENT,
    `class_id` bigint(20)   NOT NULL,
    `uri`      varchar(200) NOT NULL,
    `name`     varchar(100) NOT NULL,
    `type`     varchar(50)  NOT NULL,
    `size`     bigint(20)   NOT NULL,
    `time`     datetime     NOT NULL,
    PRIMARY KEY (`file_id`),
    KEY `FK_file_class` (`class_id`),
    CONSTRAINT `FK_file_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = latin1;

-- Dumping data for table e-tutoring.file: ~0 rows (approximately)
DELETE
FROM `file`;
/*!40000 ALTER TABLE `file`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `file`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.message
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message`
(
    `message_id` bigint(20)   NOT NULL AUTO_INCREMENT,
    `class_id`   bigint(20)   NOT NULL,
    `time`       datetime     NOT NULL,
    `content`    varchar(500) NOT NULL,
    `sender`     varchar(500) NOT NULL,
    PRIMARY KEY (`message_id`),
    KEY `FK_message_class` (`class_id`),
    CONSTRAINT `FK_message_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 258
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.message: ~256 rows (approximately)
DELETE
FROM `message`;
/*!40000 ALTER TABLE `message`
    DISABLE KEYS */;
INSERT INTO `message` (`message_id`, `class_id`, `time`, `content`, `sender`)
VALUES (2, 33, '2020-03-28 00:00:00', 'abc', 'GBH1691'),
       (3, 33, '2020-03-28 00:00:00', 'abc', 'BA001'),
       (4, 33, '2020-03-28 00:00:00', 'bcd', 'GBH1691'),
       (5, 33, '2020-03-28 00:00:00', 'bcd', 'BA001'),
       (6, 17, '2020-03-28 00:00:00', 'abc', 'BA002'),
       (7, 17, '2020-03-28 00:00:00', 'abc', 'GBH1692'),
       (8, 17, '2020-03-28 00:00:00', 'bcd', 'GBH1692'),
       (9, 17, '2020-03-28 00:00:00', 'bcd', 'BA002'),
       (10, 17, '2020-03-28 00:00:00', 'cde', 'GBH1692'),
       (11, 33, '2020-04-30 19:02:48', 'hello guy', 'BA001'),
       (12, 48, '2020-05-04 06:44:41', 'Hi there', 'BA001'),
       (13, 48, '2020-05-04 06:45:52', 'Hi how are you today?', 'GBH1702'),
       (14, 48, '2020-05-04 06:46:01', 'I\'m fine', 'BA001'),
       (15, 48, '2020-05-04 06:46:21', 'Today, i have problem want share to you', 'BA001'),
       (16, 48, '2020-05-04 06:46:58', 'Oke. But now i\'m busy. So you can send to me later', 'GBH1702'),
       (17, 48, '2020-05-04 06:47:17', 'Oke ', 'BA001'),
       (18, 39, '2020-05-04 06:49:03', 'Hello', 'GBH1703'),
       (19, 39, '2020-05-04 06:49:27', 'What up', 'GD001'),
       (20, 39, '2020-05-04 06:51:07', ' this.router.navigate([\'/student/Dashboard\'])', 'GD001'),
       (21, 39, '2020-05-04 06:51:54', 'Hello', 'GD001'),
       (22, 39, '2020-05-04 06:51:59', 'Hello', 'GBH1703'),
       (23, 39, '2020-05-04 06:52:08', 'How are you?', 'GD001'),
       (24, 39, '2020-05-04 06:52:15', 'I’m good. How are you?', 'GBH1703'),
       (25, 39, '2020-05-04 06:52:25', 'Good. Do you speak English?', 'GD001'),
       (26, 39, '2020-05-04 06:52:35', 'A little. Are you American?', 'GBH1703'),
       (27, 39, '2020-05-04 06:52:44', 'Yes.', 'GD001'),
       (28, 39, '2020-05-04 06:52:52', 'Where are you from?', 'GBH1703'),
       (29, 39, '2020-05-04 06:53:00', 'I’m from California.', 'GD001'),
       (30, 39, '2020-05-04 06:53:10', 'Nice to meet you.', 'GBH1703'),
       (31, 39, '2020-05-04 06:53:19', 'Nice to meet you too.', 'GD001'),
       (32, 33, '2020-05-04 06:55:32', 'Hello sir, welcome to the French Garden Restaurant. How many?', 'GBH1691'),
       (33, 33, '2020-05-04 06:55:37', 'One.', 'BA001'),
       (34, 33, '2020-05-04 06:55:44',
        'Right this way. Please have a seat. Your waitress will be with you in a moment.', 'GBH1691'),
       (35, 33, '2020-05-04 06:55:50', 'Hello sir, would you like to order now?', 'BA001'),
       (36, 33, '2020-05-04 06:55:55', 'Yes please.', 'GBH1691'),
       (37, 33, '2020-05-04 06:56:01', 'What would you like to drink?', 'BA001'),
       (38, 33, '2020-05-04 06:56:22', 'puzzy drink', 'GBH1691'),
       (39, 33, '2020-05-04 06:56:40', 'I’ll have a bottle of water please.', 'BA001'),
       (40, 33, '2020-05-04 06:57:03', 'What would you like to eat?', 'GBH1691'),
       (41, 33, '2020-05-04 06:57:18', 'I’ll have a tuna fish sandwich and a bowl of vegetable soup.', 'BA001'),
       (42, 51, '2020-05-04 06:58:47', 'I’ll have a tuna fish sandwich and a bowl of vegetable soup.', 'BA001'),
       (43, 51, '2020-05-04 06:59:06', 'Excuse me.', 'GBH1693'),
       (44, 51, '2020-05-04 06:59:15', 'Hello sir, may I help you?', 'BA001'),
       (45, 51, '2020-05-04 06:59:22', 'Yes. Can I see that shirt on the top shelf please?', 'GBH1693'),
       (46, 51, '2020-05-04 06:59:32', 'Sure. Here it is.', 'BA001'),
       (47, 51, '2020-05-04 06:59:39', 'How much does it cost?', 'GBH1693'),
       (48, 51, '2020-05-04 06:59:47', '50 dollars.', 'BA001'),
       (49, 51, '2020-05-04 06:59:55', '50 dollars. That’s too much.', 'GBH1693'),
       (50, 51, '2020-05-04 07:00:03', 'How about this one? It’s on sale for only 35 dollars.', 'BA001'),
       (51, 51, '2020-05-04 07:00:09', 'I don’t like that one.', 'GBH1693'),
       (52, 51, '2020-05-04 07:00:17',
        'How about the one next to the black gloves? It’s very similar to the one you like.', 'BA001'),
       (53, 51, '2020-05-04 07:00:28', 'That’s nice. How much is it?', 'GBH1693'),
       (54, 51, '2020-05-04 07:00:36', '30 dollars.', 'BA001'),
       (55, 51, '2020-05-04 07:00:47', 'That’ll be fine.', 'GBH1693'),
       (56, 51, '2020-05-04 07:00:54', 'Is this color OK, or would you like a different color?', 'BA001'),
       (57, 51, '2020-05-04 07:01:00', 'That blue one’s fine.', 'GBH1693'),
       (58, 51, '2020-05-04 07:01:07', 'Do you need any more of these shirts?', 'BA001'),
       (59, 51, '2020-05-04 07:01:14', 'Yes.', 'GBH1693'),
       (60, 51, '2020-05-04 07:01:21', 'How many do you want?', 'BA001'),
       (61, 51, '2020-05-04 07:01:27', 'I’ll take two more, a red one and a white one.', 'GBH1693'),
       (62, 43, '2020-05-04 07:03:12', 'Hello?', 'GBH1708'),
       (63, 43, '2020-05-04 07:03:18', 'Hi, is James there please?', 'IT002'),
       (64, 43, '2020-05-04 07:03:23', 'Yes. Who’s calling?', 'GBH1708'),
       (65, 43, '2020-05-04 07:03:30', 'Linda.', 'IT002'),
       (66, 43, '2020-05-04 07:04:26', 'One moment please.', 'GBH1708'),
       (67, 43, '2020-05-04 07:04:32', 'Hello?', 'IT002'),
       (68, 43, '2020-05-04 07:04:38', 'Hi James, it’s Linda.', 'GBH1708'),
       (69, 43, '2020-05-04 07:04:45', 'Hi Linda.', 'IT002'),
       (70, 43, '2020-05-04 07:05:10', 'What are you doing now?', 'GBH1708'),
       (71, 43, '2020-05-04 07:05:17', 'I’m working.', 'IT002'),
       (72, 43, '2020-05-04 07:05:22', 'Are you busy?', 'GBH1708'),
       (73, 43, '2020-05-04 07:05:29', 'Yes. It’s been really busy here all day.', 'IT002'),
       (74, 43, '2020-05-04 07:05:36', 'What time do you get off (of) work?', 'GBH1708'),
       (75, 43, '2020-05-04 07:05:45', '8:30PM', 'IT002'),
       (76, 37, '2020-05-04 07:12:33', 'Brian, do you know how to speak English?', 'IT001'),
       (77, 37, '2020-05-04 07:12:43', 'Yes.', 'GBH1712'),
       (78, 37, '2020-05-04 07:12:50', 'Where did you learn?', 'IT001'),
       (79, 37, '2020-05-04 07:12:57', 'I learned in college.', 'GBH1712'),
       (80, 37, '2020-05-04 07:13:02', 'You speak really well.', 'IT001'),
       (81, 37, '2020-05-04 07:13:10', 'Thank you.', 'GBH1712'),
       (82, 37, '2020-05-04 07:13:17', 'How long have you been in the US?', 'IT001'),
       (83, 37, '2020-05-04 07:13:23', '3 weeks.', 'GBH1712'),
       (84, 37, '2020-05-04 07:13:29', 'Is your wife with you?', 'IT001'),
       (85, 37, '2020-05-04 07:13:35', 'Yes, she just got here yesterday.', 'GBH1712'),
       (86, 37, '2020-05-04 07:13:43', 'Have you been to California before?', 'IT001'),
       (87, 37, '2020-05-04 07:13:52', 'No. I’ve never been there.', 'GBH1712'),
       (88, 37, '2020-05-04 07:13:58', 'Have you ever been to Las Vegas?', 'IT001'),
       (89, 37, '2020-05-04 07:14:04', 'Yes. I went there once on a business trip.', 'GBH1712'),
       (90, 45, '2020-05-04 07:23:18', 'David, would you like something to eat?', 'GD003'),
       (91, 45, '2020-05-04 07:23:28', 'No, I\'m full.', 'GD003'),
       (92, 45, '2020-05-04 07:23:37', 'No, I\'m full.', 'GBH1701'),
       (93, 45, '2020-05-04 07:23:48', 'Do you want something to drink?', 'GD003'),
       (94, 45, '2020-05-04 07:23:55', 'Yes, I\'d like some coffee.', 'GBH1701'),
       (95, 45, '2020-05-04 07:24:01', 'Sorry, I don\'t have any coffee.', 'GD003'),
       (96, 45, '2020-05-04 07:24:06', 'That\'s OK. I\'ll have a glass of water.', 'GBH1701'),
       (97, 45, '2020-05-04 07:24:13', 'A small glass, or a big one?', 'GD003'),
       (98, 45, '2020-05-04 07:24:23', 'Small please.', 'GBH1701'),
       (99, 45, '2020-05-04 07:24:29', 'Here you go.', 'GD003'),
       (100, 45, '2020-05-04 07:25:16', 'Thanks.', 'GBH1701'),
       (101, 45, '2020-05-04 07:25:23', 'You\'re welcome.', 'GD003'),
       (102, 17, '2020-05-04 07:26:49', 'Mary, would you like to get something to eat with me?', 'GBH1692'),
       (103, 17, '2020-05-04 07:28:02', 'OK. When?', 'BA002'),
       (104, 17, '2020-05-04 07:28:17', 'At 10 O\'clock', 'GBH1692'),
       (105, 17, '2020-05-04 07:28:24', '10 in the morning?', 'BA002'),
       (106, 17, '2020-05-04 07:28:31', 'No, at night.', 'GBH1692'),
       (107, 17, '2020-05-04 07:28:37', 'Sorry, that\'s too late. I usually go to bed around 10:00PM', 'BA002'),
       (108, 17, '2020-05-04 07:28:44', 'OK, how about 1:30 PM?', 'GBH1692'),
       (109, 17, '2020-05-04 07:28:51', 'No, that\'s too early. I\'ll still be at work then.', 'BA002'),
       (110, 17, '2020-05-04 07:28:58', 'How about 5:00PM?', 'GBH1692'),
       (111, 17, '2020-05-04 07:29:06', 'That\'s fine.', 'BA002'),
       (112, 17, '2020-05-04 07:29:13', 'OK, see you then.', 'GBH1692'),
       (113, 17, '2020-05-04 07:29:20', 'Alright. Bye.', 'BA002'),
       (114, 44, '2020-05-04 07:38:01', 'Hi Michael.', 'GBH1706'),
       (115, 44, '2020-05-04 07:38:10', 'Hi Amy. What\'s up?', 'GD002'),
       (116, 44, '2020-05-04 07:38:18', 'I\'m looking for the airport. Can you tell me how to get there?', 'GBH1706'),
       (117, 44, '2020-05-04 07:38:25', 'No, sorry. I don\'t know.', 'GD002'),
       (118, 44, '2020-05-04 07:38:31',
        'I think I can take the subway to the airport. Do you know where the subway is?', 'GBH1706'),
       (119, 44, '2020-05-04 07:38:55', 'Sure, it\'s over there.', 'GD002'),
       (120, 44, '2020-05-04 07:39:08', 'Where? I don\'t see it.', 'GBH1706'),
       (121, 44, '2020-05-04 07:39:15', 'Across the street.', 'GD002'),
       (122, 44, '2020-05-04 07:39:21', 'Oh, I see it now. Thanks.', 'GBH1706'),
       (123, 44, '2020-05-04 07:39:26', 'No problem.', 'GD002'),
       (124, 44, '2020-05-04 07:39:33', 'Do you know if there\'s a restroom around here?', 'GBH1706'),
       (125, 44, '2020-05-04 07:39:40', 'Yes, there\'s one here. It\'s in the store.', 'GD002'),
       (126, 44, '2020-05-04 07:39:48', 'Thank you.', 'GBH1706'),
       (127, 44, '2020-05-04 07:39:54', 'Bye.', 'GBH1706'),
       (128, 44, '2020-05-04 07:40:00', 'Bye bye', 'GBH1706'),
       (129, 44, '2020-05-04 07:40:05', 'Bye bye', 'GD002'),
       (130, 42, '2020-05-04 07:45:29', 'Jennifer, would you like to have dinner with me?', 'GD004'),
       (131, 42, '2020-05-04 07:45:38', 'Yes. That would be nice. When do you want to go?', 'GBH1700'),
       (132, 42, '2020-05-04 07:45:45', 'Is today OK?', 'GD004'),
       (133, 42, '2020-05-04 07:45:52', 'Sorry, I can\'t go today.', 'GBH1700'),
       (134, 42, '2020-05-04 07:46:01', 'How about tomorrow night?', 'GD004'),
       (135, 42, '2020-05-04 07:46:07', 'Ok. What time?', 'GBH1700'),
       (136, 42, '2020-05-04 07:46:15', 'Is 9:00PM all right?', 'GD004'),
       (137, 42, '2020-05-04 07:46:21', 'I think that\'s too late.', 'GBH1700'),
       (138, 42, '2020-05-04 07:46:31', 'Is 6:00PM OK?', 'GD004'),
       (139, 42, '2020-05-04 07:46:38', 'Yes, that\'s good. Where would you like to go?', 'GBH1700'),
       (140, 42, '2020-05-04 07:46:47', 'The Italian restaurant on 5th street.', 'GD004'),
       (141, 42, '2020-05-04 07:46:55', 'Oh, I don\'t like that Restaurant. I don\'t want to go there.', 'GBH1700'),
       (142, 42, '2020-05-04 07:47:02', 'How about the Korean restaurant next to it?', 'GD004'),
       (143, 42, '2020-05-04 07:47:10', 'OK, I like that place.', 'GD004'),
       (144, 40, '2020-05-04 08:03:32', 'Hello sir, welcome to the French Garden Restaurant. How many?', 'GBH1699'),
       (145, 40, '2020-05-04 08:03:41', 'One.', 'IT003'),
       (146, 40, '2020-05-04 08:03:48',
        'Right this way. Please have a seat. Your waitress will be with you in a moment.', 'GBH1699'),
       (147, 40, '2020-05-04 08:03:54', 'Hello sir, would you like to order now?', 'GBH1699'),
       (148, 40, '2020-05-04 08:04:03', 'Yes please.', 'IT003'),
       (149, 40, '2020-05-04 08:04:09', 'What would you like to drink?', 'GBH1699'),
       (150, 40, '2020-05-04 08:04:16', 'What do you have?', 'IT003'),
       (151, 40, '2020-05-04 08:04:23', 'We have bottled water, juice, and Coke.', 'GBH1699'),
       (152, 40, '2020-05-04 08:04:29', 'I\'ll have a bottle of water please.', 'IT003'),
       (153, 40, '2020-05-04 08:04:43', 'What would you like to eat?', 'GBH1699'),
       (154, 40, '2020-05-04 08:04:50', 'I\'ll have a tuna fish sandwich and a bowl of vegetable soup.', 'IT003'),
       (155, 49, '2020-05-04 08:06:48', 'Chris, where are you going?', 'GBH1710'),
       (156, 49, '2020-05-04 08:07:27', 'I\'m going to the store. I need to buy something.', 'IT004'),
       (157, 49, '2020-05-04 08:07:34', 'Really? I need to go to the store too.', 'GBH1710'),
       (158, 49, '2020-05-04 08:07:41', 'Would you like to come with me?', 'IT004'),
       (159, 49, '2020-05-04 08:07:47', 'Yeah, let\'s go together', 'GBH1710'),
       (160, 49, '2020-05-04 08:07:53', 'Would you like to go now or later?', 'IT004'),
       (161, 49, '2020-05-04 08:07:59', 'Now.', 'GBH1710'),
       (162, 49, '2020-05-04 08:08:05', 'What?', 'IT004'),
       (163, 49, '2020-05-04 08:08:13', 'Now would be better.', 'GBH1710'),
       (164, 49, '2020-05-04 08:08:21', 'OK, let\'s go.', 'IT004'),
       (165, 49, '2020-05-04 08:08:27', 'Should we walk?', 'GBH1710'),
       (166, 49, '2020-05-04 08:08:36', 'No, it\'s too far. Let\'s drive.', 'IT004'),
       (167, 41, '2020-05-04 08:11:21', 'Laura, what are you going to do today?', 'IT005'),
       (168, 41, '2020-05-04 08:11:30', 'I\'m going shopping.', 'GBH1714'),
       (169, 41, '2020-05-04 08:11:37', 'What time are you leaving?', 'IT005'),
       (170, 41, '2020-05-04 08:11:42', 'I\'m going to leave around 4 o\'clock.', 'GBH1714'),
       (171, 41, '2020-05-04 08:11:48', 'Will you buy a ham sandwich for me at the store?', 'IT005'),
       (172, 41, '2020-05-04 08:11:53', 'OK.', 'GBH1714'),
       (173, 41, '2020-05-04 08:11:59', 'Do you have enough money?', 'IT005'),
       (174, 41, '2020-05-04 08:12:04', 'I\'m not sure.', 'GBH1714'),
       (175, 41, '2020-05-04 08:12:10', 'How much do you have?', 'IT005'),
       (176, 41, '2020-05-04 08:12:16', '25 dollars. Do you think that\'s enough?', 'GBH1714'),
       (177, 41, '2020-05-04 08:12:29', 'That\'s not very much.', 'IT005'),
       (178, 41, '2020-05-04 08:12:34', 'I think it\'s OK. I also have two credit cards.', 'GBH1714'),
       (179, 41, '2020-05-04 08:12:40', 'Let me give you another ten dollars.', 'IT005'),
       (180, 41, '2020-05-04 08:12:47', 'Thanks. See you later.', 'GBH1714'),
       (181, 41, '2020-05-04 08:12:54', 'Bye.', 'IT005'),
       (182, 46, '2020-05-04 08:15:29', 'Hello Richard.', 'GD005'),
       (183, 46, '2020-05-04 08:15:37', 'Hi Karen. How have you been?', 'GBH1711'),
       (184, 46, '2020-05-04 08:15:42', 'Not too good.', 'GD005'),
       (185, 46, '2020-05-04 08:15:48', 'Why?', 'GBH1711'),
       (186, 46, '2020-05-04 08:15:55', 'I\'m sick.', 'GD005'),
       (187, 46, '2020-05-04 08:16:01', 'Sorry to hear that', 'GBH1711'),
       (188, 46, '2020-05-04 08:16:06', 'It\'s OK. It\'s not serious.', 'GD005'),
       (189, 46, '2020-05-04 08:16:12', 'That\'s good. How\'s your wife?', 'GBH1711'),
       (190, 46, '2020-05-04 08:16:17', 'She\'s good.', 'GD005'),
       (191, 46, '2020-05-04 08:16:24', 'Is she in America now?', 'GBH1711'),
       (192, 46, '2020-05-04 08:16:30', 'No, she\'s not here yet.', 'GD005'),
       (193, 46, '2020-05-04 08:16:36', 'Where is she?', 'GBH1711'),
       (194, 46, '2020-05-04 08:16:41', 'She\'s in Canada with our kids.', 'GD005'),
       (195, 46, '2020-05-04 08:16:47', 'I see. I have to go now. Please tell your wife I said hi.', 'GBH1711'),
       (196, 46, '2020-05-04 08:16:52', 'OK, I\'ll talk to you later.', 'GD005'),
       (197, 46, '2020-05-04 08:16:59', 'I hope you feel better', 'GBH1711'),
       (198, 38, '2020-05-04 08:18:07', 'Robert, this is my friend, Mrs. Smith.', 'BA005'),
       (199, 38, '2020-05-04 08:18:12', 'Hi, Nice to meet you.', 'GBH1696'),
       (200, 38, '2020-05-04 08:18:19', 'Nice to meet you too.', 'BA005'),
       (201, 38, '2020-05-04 08:18:24', 'Mrs. Smith, what do you do for work?', 'GBH1696'),
       (202, 38, '2020-05-04 08:18:30', 'I\'m a doctor.', 'BA005'),
       (203, 38, '2020-05-04 08:18:36', 'Oh. Where do you work?', 'GBH1696'),
       (204, 38, '2020-05-04 08:18:42', 'New York University hospital in New York City. What do you do?', 'BA005'),
       (205, 38, '2020-05-04 08:18:47', 'I\'m a teacher.', 'GBH1696'),
       (206, 38, '2020-05-04 08:18:53', 'What do you teach?', 'BA005'),
       (207, 38, '2020-05-04 08:18:58', 'I teach English.', 'GBH1696'),
       (208, 38, '2020-05-04 08:19:03', 'Where?', 'BA005'),
       (209, 38, '2020-05-04 08:19:09', 'At a high school in New Jersey.', 'GBH1696'),
       (210, 38, '2020-05-04 08:19:14', 'That\'s nice. How old are you?', 'BA005'),
       (211, 38, '2020-05-04 08:19:20', 'I\'m 32.', 'GBH1696'),
       (212, 35, '2020-05-04 08:27:44', 'Excuse me, I\'m looking for the Holiday Inn. Do you know where it is?',
        'BA003'),
       (213, 35, '2020-05-04 08:27:49', 'Sure. It\'s down this street on the left.', 'GBH1697'),
       (214, 35, '2020-05-04 08:27:55', 'Is it far from here?', 'BA003'),
       (215, 35, '2020-05-04 08:28:00', 'No, it\'s not far.', 'GBH1697'),
       (216, 35, '2020-05-04 08:28:05', 'How far is it?', 'BA003'),
       (217, 35, '2020-05-04 08:28:12', 'About a mile and a half.', 'GBH1697'),
       (218, 35, '2020-05-04 08:28:17', 'How long does it take to get there?', 'BA003'),
       (219, 35, '2020-05-04 08:28:23', '5 minutes or so.', 'GBH1697'),
       (220, 35, '2020-05-04 08:28:28', 'Is it close to the subway station?', 'BA003'),
       (221, 35, '2020-05-04 08:28:35',
        'Yes, it\'s very close. The subway station is next to the hotel. You can walk there.', 'GBH1697'),
       (222, 35, '2020-05-04 08:28:45', 'Thanks a lot.', 'BA003'),
       (223, 36, '2020-05-04 08:33:31', 'Hi Sarah, how are you?', 'BA003'),
       (224, 36, '2020-05-04 08:33:39', 'Fine, how are you doing?', 'GBH1709'),
       (225, 36, '2020-05-04 08:33:44', 'OK.', 'BA003'),
       (226, 36, '2020-05-04 08:33:50', 'What do you want to do?', 'GBH1709'),
       (227, 36, '2020-05-04 08:34:00', 'I\'m hungry. I\'d like to eat something.', 'BA003'),
       (228, 36, '2020-05-04 08:34:09', 'Where do you want to go?', 'GBH1709'),
       (229, 36, '2020-05-04 08:34:17', 'I\'d like to go to an Italian restaurant.', 'BA003'),
       (230, 36, '2020-05-04 08:34:22', 'What kind of Italian food do you like?', 'GBH1709'),
       (231, 36, '2020-05-04 08:34:28', 'I like spaghetti. Do you like spaghetti?', 'BA003'),
       (232, 36, '2020-05-04 08:34:37', 'No, I don\'t, but I like pizza.', 'GBH1709'),
       (233, 17, '2020-05-04 16:42:02', 'Hello', 'GBH1692'),
       (234, 17, '2020-05-04 16:42:21', 'Hi! How are you today?', 'BA002'),
       (235, 17, '2020-05-04 19:31:26', 'Hi tutor', 'GBH1692'),
       (236, 17, '2020-05-04 19:31:42', 'Hello', 'BA002'),
       (237, 17, '2020-05-04 19:32:37', 'I have some problem in process code', 'GBH1692'),
       (238, 17, '2020-05-04 19:32:51', 'Oh! Can i have you', 'BA002'),
       (239, 17, '2020-05-04 20:03:26', 'Hello student', 'BA002'),
       (240, 17, '2020-05-04 20:03:52', 'Hi Tutor. How are you today.', 'GBH1692'),
       (241, 33, '2020-05-06 14:57:09', 'abc', 'BA001'),
       (242, 33, '2020-05-06 14:58:46', 'abc', 'GBH1691'),
       (243, 33, '2020-05-06 14:59:19', 'abc', 'GBH1691'),
       (244, 33, '2020-05-06 14:59:24', 'abc', 'BA001'),
       (245, 33, '2020-05-06 14:59:29', '?d', 'BA001'),
       (246, 33, '2020-05-06 14:59:30', '?dawd', 'BA001'),
       (247, 33, '2020-05-06 14:59:30', '??', 'BA001'),
       (248, 33, '2020-05-06 14:59:31', '?d', 'BA001'),
       (249, 33, '2020-05-06 14:59:32', '?', 'BA001'),
       (250, 33, '2020-05-06 14:59:38', '12', 'GBH1691'),
       (251, 33, '2020-05-06 14:59:38', '312', 'GBH1691'),
       (252, 33, '2020-05-06 14:59:38', '1', 'GBH1691'),
       (253, 33, '2020-05-06 14:59:39', '5313', 'GBH1691'),
       (254, 33, '2020-05-06 14:59:39', '1', 'GBH1691'),
       (255, 33, '2020-05-06 14:59:40', '2', 'GBH1691'),
       (256, 33, '2020-05-06 14:59:40', '5121', 'GBH1691'),
       (257, 33, '2020-05-06 14:59:42', '513', 'GBH1691');
/*!40000 ALTER TABLE `message`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.schedule
DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule`
(
    `schedule_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `class_id`    bigint(20) NOT NULL,
    `date`        date       NOT NULL,
    `slot_1`      tinyint(1) DEFAULT 0,
    `slot_2`      tinyint(1) DEFAULT 0,
    `slot_3`      tinyint(1) DEFAULT 0,
    `slot_4`      tinyint(1) DEFAULT 0,
    `slot_5`      tinyint(1) DEFAULT 0,
    `slot_6`      tinyint(1) DEFAULT 0,
    `slot_7`      tinyint(1) DEFAULT 0,
    `slot_8`      tinyint(1) DEFAULT 0,
    PRIMARY KEY (`schedule_id`),
    KEY `FK_attendence_class` (`class_id`),
    CONSTRAINT `FK_attendence_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 159
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.schedule: ~151 rows (approximately)
DELETE
FROM `schedule`;
/*!40000 ALTER TABLE `schedule`
    DISABLE KEYS */;
INSERT INTO `schedule` (`schedule_id`, `class_id`, `date`, `slot_1`, `slot_2`, `slot_3`, `slot_4`, `slot_5`, `slot_6`,
                        `slot_7`, `slot_8`)
VALUES (5, 17, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (6, 17, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (7, 17, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (8, 17, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (9, 17, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (10, 17, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (11, 17, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (12, 35, '2020-04-13', 0, 1, 0, 0, 0, 0, 0, 0),
       (13, 35, '2020-04-14', 0, 0, 1, 0, 0, 0, 0, 0),
       (14, 35, '2020-04-15', 0, 0, 0, 1, 0, 0, 0, 1),
       (15, 35, '2020-04-16', 0, 0, 0, 0, 1, 0, 0, 0),
       (16, 35, '2020-04-17', 0, 0, 0, 0, 0, 1, 0, 0),
       (17, 35, '2020-04-18', 0, 0, 0, 0, 0, 0, 1, 0),
       (18, 35, '2020-04-19', 0, 0, 0, 0, 0, 0, 0, 1),
       (19, 36, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (20, 36, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (21, 36, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (22, 36, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (23, 36, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (24, 36, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (25, 36, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (26, 37, '2020-04-13', 0, 0, 0, 0, 1, 0, 0, 0),
       (27, 37, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (28, 37, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (29, 37, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (30, 37, '2020-04-17', 1, 0, 0, 0, 0, 0, 0, 0),
       (31, 37, '2020-04-18', 0, 0, 0, 0, 1, 0, 0, 0),
       (32, 37, '2020-04-19', 0, 0, 0, 0, 1, 0, 0, 0),
       (33, 38, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (34, 38, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (35, 38, '2020-04-15', 1, 0, 0, 0, 0, 0, 0, 0),
       (36, 38, '2020-04-16', 0, 0, 1, 0, 0, 0, 0, 0),
       (37, 38, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (38, 38, '2020-04-18', 0, 0, 1, 0, 0, 0, 0, 0),
       (39, 38, '2020-04-19', 0, 0, 0, 1, 0, 0, 0, 0),
       (40, 39, '2020-04-13', 0, 0, 1, 0, 0, 0, 0, 0),
       (41, 39, '2020-04-14', 0, 0, 0, 1, 0, 0, 0, 0),
       (42, 39, '2020-04-15', 0, 0, 0, 0, 1, 0, 0, 0),
       (43, 39, '2020-04-16', 0, 0, 0, 0, 0, 1, 0, 0),
       (44, 39, '2020-04-17', 0, 0, 1, 0, 0, 0, 0, 0),
       (45, 39, '2020-04-18', 0, 1, 0, 0, 0, 0, 0, 0),
       (46, 39, '2020-04-19', 0, 0, 0, 1, 0, 0, 0, 0),
       (47, 40, '2020-04-13', 0, 0, 0, 0, 0, 0, 1, 0),
       (48, 40, '2020-04-14', 0, 0, 0, 0, 1, 0, 0, 0),
       (49, 40, '2020-04-15', 0, 1, 0, 0, 0, 0, 0, 0),
       (50, 40, '2020-04-16', 1, 0, 0, 0, 0, 0, 0, 0),
       (51, 40, '2020-04-17', 0, 0, 0, 1, 0, 0, 0, 0),
       (52, 40, '2020-04-18', 0, 0, 0, 0, 1, 0, 0, 0),
       (53, 40, '2020-04-19', 0, 0, 1, 0, 0, 0, 0, 0),
       (54, 41, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (55, 41, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (56, 41, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (57, 41, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (58, 41, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (59, 41, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (60, 41, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (61, 42, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (62, 42, '2020-04-14', 0, 0, 1, 0, 0, 0, 0, 0),
       (63, 42, '2020-04-15', 0, 1, 0, 0, 0, 0, 0, 0),
       (64, 42, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (65, 42, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (66, 42, '2020-04-18', 0, 0, 0, 0, 0, 0, 1, 0),
       (67, 42, '2020-04-19', 0, 0, 0, 0, 0, 1, 0, 0),
       (68, 43, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (69, 43, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (70, 43, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (71, 43, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (72, 43, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (73, 43, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (74, 43, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (75, 44, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (76, 44, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (77, 44, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (78, 44, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (79, 44, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (80, 44, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (81, 44, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (82, 45, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (83, 45, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (84, 45, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (85, 45, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (86, 45, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (87, 45, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (88, 45, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (89, 46, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (90, 46, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (91, 46, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (92, 46, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (93, 46, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (94, 46, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (95, 46, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (96, 47, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (97, 47, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (98, 47, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (99, 47, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (100, 47, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (101, 47, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (102, 47, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (103, 48, '2020-04-13', 0, 1, 0, 0, 0, 0, 0, 0),
       (104, 48, '2020-04-14', 0, 0, 1, 0, 0, 0, 0, 0),
       (105, 48, '2020-04-15', 0, 0, 0, 1, 0, 0, 0, 0),
       (106, 48, '2020-04-16', 0, 0, 0, 0, 1, 0, 0, 0),
       (107, 48, '2020-04-17', 0, 0, 0, 0, 0, 1, 0, 0),
       (108, 48, '2020-04-18', 0, 0, 0, 0, 0, 0, 1, 0),
       (109, 48, '2020-04-19', 0, 0, 0, 0, 0, 0, 0, 1),
       (110, 49, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (111, 49, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (112, 49, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (113, 49, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (114, 49, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (115, 49, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (116, 49, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (117, 33, '2020-04-13', 1, 0, 0, 0, 0, 0, 0, 0),
       (118, 33, '2020-04-14', 0, 1, 0, 0, 0, 0, 0, 0),
       (119, 33, '2020-04-15', 0, 0, 1, 0, 0, 0, 0, 0),
       (120, 33, '2020-04-16', 0, 0, 0, 1, 0, 0, 0, 0),
       (121, 33, '2020-04-17', 0, 0, 0, 0, 1, 0, 0, 0),
       (122, 33, '2020-04-18', 0, 0, 0, 0, 0, 1, 0, 0),
       (123, 33, '2020-04-19', 0, 0, 0, 0, 0, 0, 1, 0),
       (125, 47, '2020-04-20', 1, 0, 0, 0, 0, 0, 0, 0),
       (126, 47, '2020-04-21', 0, 1, 0, 0, 0, 0, 0, 0),
       (128, 47, '2020-04-22', 0, 0, 1, 0, 0, 0, 0, 0),
       (129, 47, '2020-04-23', 0, 1, 0, 0, 0, 0, 0, 0),
       (130, 47, '2020-04-24', 0, 0, 1, 0, 0, 0, 0, 0),
       (131, 47, '2020-04-25', 0, 1, 0, 0, 0, 0, 0, 0),
       (132, 47, '2020-04-26', 0, 0, 1, 0, 0, 0, 0, 0),
       (133, 48, '2020-04-20', 1, 0, 0, 0, 0, 0, 0, 0),
       (134, 48, '2020-04-21', 0, 1, 0, 0, 0, 0, 0, 0),
       (135, 48, '2020-04-22', 1, 0, 0, 0, 0, 0, 0, 0),
       (136, 48, '2020-04-23', 0, 1, 0, 0, 0, 0, 0, 0),
       (137, 48, '2020-04-24', 1, 0, 0, 0, 0, 0, 0, 0),
       (138, 48, '2020-04-25', 0, 1, 0, 0, 0, 0, 0, 0),
       (139, 48, '2020-04-26', 1, 0, 0, 0, 0, 0, 0, 0),
       (140, 17, '2020-04-20', 1, 0, 0, 0, 0, 0, 0, 0),
       (141, 17, '2020-04-21', 0, 1, 0, 0, 0, 0, 0, 0),
       (142, 17, '2020-04-22', 0, 0, 1, 0, 0, 0, 0, 0),
       (143, 17, '2020-04-23', 0, 0, 0, 1, 0, 0, 0, 0),
       (144, 17, '2020-04-24', 0, 0, 1, 0, 0, 0, 0, 0),
       (145, 17, '2020-04-25', 0, 1, 0, 0, 0, 0, 0, 0),
       (146, 17, '2020-04-26', 1, 0, 0, 0, 0, 0, 0, 0),
       (148, 17, '2020-04-27', 1, 0, 0, 0, 0, 0, 0, 0),
       (149, 17, '2020-04-28', 0, 0, 1, 0, 0, 0, 0, 0),
       (150, 33, '2020-05-29', 0, 0, 1, 0, 0, 0, 0, 0),
       (151, 33, '2020-04-30', 0, 0, 0, 0, 0, 1, 0, 0),
       (152, 33, '2020-05-30', 0, 0, 0, 1, 0, 0, 0, 0),
       (153, 33, '2020-05-01', 0, 0, 1, 0, 0, 0, 0, 0),
       (154, 33, '2020-05-02', 0, 0, 1, 0, 0, 0, 0, 0),
       (155, 33, '2020-06-02', 1, 0, 0, 0, 0, 0, 0, 0),
       (156, 36, '2020-04-26', 0, 0, 1, 0, 0, 0, 0, 0),
       (157, 17, '2020-04-29', 0, 1, 0, 0, 0, 0, 0, 0),
       (158, 17, '2020-04-26', 0, 1, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `schedule`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.staff
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff`
(
    `staff_id`      varchar(20)  NOT NULL,
    `username`      varchar(50)  NOT NULL,
    `staff_name`    varchar(50)  NOT NULL,
    `email`         varchar(50)  NOT NULL,
    `date_of_birth` date         NOT NULL,
    `phone_number`  varchar(10)  NOT NULL,
    `address`       varchar(50)  NOT NULL,
    `avatar`        varchar(500) NOT NULL,
    PRIMARY KEY (`staff_id`),
    KEY `FK_staff_account` (`username`),
    CONSTRAINT `FK_staff_account` FOREIGN KEY (`username`) REFERENCES `account` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.staff: ~8 rows (approximately)
DELETE
FROM `staff`;
/*!40000 ALTER TABLE `staff`
    DISABLE KEYS */;
INSERT INTO `staff` (`staff_id`, `username`, `staff_name`, `email`, `date_of_birth`, `phone_number`, `address`,
                     `avatar`)
VALUES ('Staff01', 'staff', 'Trinh Van Duy', 'duytv081298@gmail.com', '1998-12-08', '0369549798', 'Ha Nam',
        'https://scontent.fsgn2-1.fna.fbcdn.net/v/t31.0-8/s960x960/25188750_426560691080581_8495229945273928165_o.jpg?_nc_cat=111&_nc_sid=7aed08&_nc_oc=AQlyCUEEDUeOmGk5g6-2TGpIYXHIGZbhWCL5B7tngY7vwwW1Bvz4OVkKw3Zzn907mf8&_nc_ht=scontent.fsgn2-1.fna&_nc_tp=7&oh=cbd3b9c4f02dbebc7507fa69a13b3663&oe=5ED5574F'),
       ('Staff02', 'LeHongThu', 'Le Hong Thu', 'thulh@gmail.com', '1998-12-06', '0987678906', 'Ha Noi',
        'https://scontent.fsgn2-2.fna.fbcdn.net/v/t1.0-9/s960x960/87958164_2224194464382841_1556291110626656256_o.jpg?_nc_cat=102&_nc_sid=8024bb&_nc_oc=AQnMEz8tiu9qHQL6EajhQd1JZe3xrq8taWz0oGg6vf8VzYq-c0Ve8jSEYCZj8TPl_yc&_nc_ht=scontent.fsgn2-2.fna&_nc_tp=7&oh=6a03bf4dd98b4448199d1aa98b3fe2ec&oe=5ED59B8E'),
       ('Staff03', 'HoangTheViet', 'Hoang The Viet', 'Vietht@gmail.com', '1998-05-05', '0987123123', 'Ha Noi',
        'https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/68328426_476659579826870_1562654297949208576_o.jpg?_nc_cat=111&_nc_sid=85a577&_nc_oc=AQkHhH3xzUu93twBpAFOVkyuSKmi_ENbn6uYsmYY3fF5fE_VpGCU5ozOKFIAy5zG1qA&_nc_ht=scontent.fsgn2-1.fna&oh=8a7f76dd8c83b35d5b4bb01926ff665b&oe=5ED4E628'),
       ('Staff04', 'VuThanhHai', 'Vu Thanh Hai', 'haivt@gmail.com', '1995-06-27', '0988888888', 'Ha Noi',
        'https://scontent.fsgn2-2.fna.fbcdn.net/v/t1.0-9/83886998_1556062077880130_7327358212934467584_o.jpg?_nc_cat=100&_nc_sid=85a577&_nc_oc=AQk5K2onS0lEVXyOPAyItuB_Mw6mP1-u4KwOZxzra1sZ3nSQBYtZs2_FX8DZy4ipDC4&_nc_ht=scontent.fsgn2-2.fna&oh=0de9e5be74506ebeff8634c54a2d24ca&oe=5ED82048'),
       ('Staff05', 'NguyenTranTuan', 'Nguyen Tran Tuan', 'Tuannt@gmail.com', '1998-05-05', '0982123321', 'Ha Noi',
        'https://scontent.fsgn2-2.fna.fbcdn.net/v/t1.0-9/30706833_623054421376595_938022395044691968_n.jpg?_nc_cat=100&_nc_sid=85a577&_nc_oc=AQmmZGpgWRT9b2eve-XOWzKNABiN5VwHf6hVUUf_kOqPV4pXXP0cM9xFuQn2GeC9v_0&_nc_ht=scontent.fsgn2-2.fna&oh=ca6eadbcbb3a738aa3e8156a5283710a&oe=5ED5B76B'),
       ('Staff06', 'NguyenThiAnhAnh', 'Nguyen Thi Anh Anh', 'Anhnta@gmail.com', '1998-05-05', '0976555666', 'Ha Noi',
        'https://scontent.fsgn2-4.fna.fbcdn.net/v/t1.0-9/p960x960/82344162_2204585263180688_3166156028395913216_o.jpg?_nc_cat=109&_nc_sid=85a577&_nc_oc=AQmM9-NySvVxW9UOSoXdVsNZwoBRNJNr1D6szGLw8liG8Q62cVpTKzrAPdJR0argXFs&_nc_ht=scontent.fsgn2-4.fna&_nc_tp=6&oh=db0644d546d4a779a0d6abba40aed84f&oe=5ED505C4'),
       ('Staff07', 'HoangNgocTuanLinh', 'Tuan Linh', 'Linhhnt@gmail.com', '1998-05-25', '0987654656', 'Ha Noi',
        'https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/14463303_1652781131717786_5425495487937735972_n.jpg?_nc_cat=107&_nc_sid=85a577&_nc_oc=AQlFis6y6RXJ1BUiVIPFZKPM7cheFlwrznTga0Sstk1VCAhQAQwyU7ZWRCyKcrnflOk&_nc_ht=scontent.fsgn2-1.fna&oh=73f3a52027b4adaffbd16e749a4c672a&oe=5ED77010'),
       ('Staff08', 'NguyenXuanSang', 'Nguyen Xuan Sang', 'Sangnx@gmail.com', '1998-05-05', '0387651213', 'Quang Ninh',
        'https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/56537510_1199633090213307_2853593073374461952_n.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_oc=AQn_XhbxiPtmbJOwD2bMxJ_o5ksS2kitPMePAFs2BQr_Wqj60Rv-rJPQIZ3Zuf86PUc&_nc_ht=scontent.fsgn2-1.fna&oh=c218d7630f90e7e2a68f3526350fb191&oe=5ED8A59D');
/*!40000 ALTER TABLE `staff`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student`
(
    `student_id`    varchar(20)  NOT NULL,
    `username`      varchar(50)  NOT NULL,
    `student_name`  varchar(50)  NOT NULL,
    `email`         varchar(50)  NOT NULL,
    `date_of_birth` date         NOT NULL,
    `phone_number`  varchar(10)  NOT NULL,
    `address`       varchar(50)  NOT NULL,
    `avatar`        varchar(500) NOT NULL,
    PRIMARY KEY (`student_id`),
    KEY `FK_student_account` (`username`),
    CONSTRAINT `FK_student_account` FOREIGN KEY (`username`) REFERENCES `account` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.student: ~24 rows (approximately)
DELETE
FROM `student`;
/*!40000 ALTER TABLE `student`
    DISABLE KEYS */;
INSERT INTO `student` (`student_id`, `username`, `student_name`, `email`, `date_of_birth`, `phone_number`, `address`,
                       `avatar`)
VALUES ('GBH1691', 'NguyenThiKhanhLy', 'Nguyen Thi Khanh Ly', 'Linhhntgch16483@fpt.edu.vn', '2000-01-05', '0982111222',
        'Lang Son', 'https://i.pinimg.com/originals/1a/b9/11/1ab911c1f900e7c7cfb44f0dcdf7b40a.jpg'),
       ('GBH1692', 'HoangThiMinh', 'Hoang Thi Minh', 'hieukhiqt2@gmail.com', '2000-03-03', '0987655666', 'Nam Dinh',
        'https://2sao.vietnamnetjsc.vn/images/2019/11/28/21/12/sana1.jpg'),
       ('GBH1693', 'PhamHoangNam', 'Pham Hoang Nam', 'hieukhiqt3@gmail.com', '1999-12-28', '0987654231', 'Hai Phong',
        'https://i.pinimg.com/originals/2d/ea/04/2dea0469a6f0b5cb7be873d919ede9a6.jpg'),
       ('GBH1694', 'NgoThiHangNga', 'Ngo Thi Hang Nga', 'hieukhiqt@gmail.com', '2000-12-11', '0989897654', 'Quang Nam',
        'https://whitedoctors.com.vn/wp-content/uploads/2018/07/9-600x450.jpg'),
       ('GBH1695', 'NguyenThiBichNgoc', 'Nguyen Thi Bich Ngoc', 'Ngocntb@gmail.com', '2000-02-26', '0395888000',
        'Quang Ninh', 'https://www.nimonews.com/wp-content/uploads/2019/12/nancy-mcdonie.jpg'),
       ('GBH1696', 'LaVanNguyen', 'La Van Nguyen', 'Nguyenlv@gmail.com', '2000-02-29', '0982334554', 'Ha Nam',
        'https://ucarecdn.audiogon.com/2108be5a-2577-4580-b660-e1fc3b4f43b0/'),
       ('GBH1697', 'PhanThiOanh', 'Phan Thi Oanh', 'Oanhpt@gmail.com', '2000-03-02', '0986666888', 'Ha Noi',
        'https://image2.tin247.com/pictures/2019/11/28/aaq1574943018.jpg'),
       ('GBH1698', 'BuiThiMinhPhuong', 'Bui Thi Minh Phuong', 'Phuongbtm@gmail.com', '2000-03-05', '0988004333',
        'Kien Giang', 'https://www.pikpng.com/pngl/m/315-3151164_twice-once-twice-dahyun-im-nayeon-twice-sana.png'),
       ('GBH1699', 'NguyenThiQuynh', 'Nguyen Thi Quynh', 'Quynhnt@gmail.com', '2000-03-12', '0903444213', 'Yen Bai',
        'https://papers.co/wallpaper/papers.co-hp73-sana-twice-girl-kpop-group-cute-40-wallpaper.jpg'),
       ('GBH1700', 'PhamThiNhuQuynh', 'Pham Thi Nhu Quynh', 'Quynhptn@gmail.com', '2000-03-10', '0986765456', 'Ha Noi',
        'https://www.nimonews.com/wp-content/uploads/2019/12/nancy-mcdonie.jpg'),
       ('GBH1701', 'NguyenThiTuoiTham', 'Nguyen Thi Tuoi Tham', 'Thamntt@gmail.com', '2000-03-02', '0981333444',
        'Hai Duong',
        'https://nguoi-noi-tieng.com/images/post/hot-girl-dinh-dam-thai-lan-sieu-de-thuong-khi-cover-em-gai-mua-trong-oto-557006.jpg'),
       ('GBH1702', 'VuThiThuy', 'Vu Thi Thuy', 'hieukhiqt4@gmail.com', '2000-03-05', '0987654321', 'Hai Duong',
        'https://media.giadinhmoi.vn/files/thutrang/2018/03/30/phuong-vy-1-1232.jpg'),
       ('GBH1703', 'NguyenThiThuy', 'Nguyen Thi Thuy', 'Thuynt@gmail.com', '2000-03-07', '0912456565', 'Nam Dinh',
        'https://vnn-imgs-a1.vgcloud.vn/znews-photo.zadn.vn/w660/Uploaded/mdf_uswreo/2019_07_08/_trangthuy_33816048_200282977280433_354888753916936192_n.jpg'),
       ('GBH1704', 'NguyenThiThanhThuy', 'Nguyen Thi Thanh Thuy', 'Thuyntt@gmail.com', '2000-03-04', '0965666888',
        'Ha Noi', 'https://img.docbao.vn/images/fullsize/2019/02/25/hot-girl.jpg'),
       ('GBH1705', 'DoThiTrang', 'Do Thi Trang', 'Trangdt@gmail.com', '2000-03-02', '0987689999', 'Ha Noi',
        'https://www.hashatit.com/images/uploads/users/39803/profile_picture/c52f3d7acce8db8bed5d4327f6a51de61.jpg'),
       ('GBH1706', 'HuaThiThuTrang', 'Hua Thi Thu Trang', 'Tranghtt@gmail.com', '2000-07-07', '0987676765', 'Nam Dinh',
        'https://media.doisongvietnam.vn/u/rootimage/editor/2019/10/06/20/21/11570346463_8723.jpg'),
       ('GBH1707', 'NguyenThiTrang', 'Nguyen Thi Trang', 'Trangnt@gmail.com', '2000-08-02', '0988999655', 'Ha Nam',
        'https://i.imgur.com/urPD7tc.jpg'),
       ('GBH1708', 'PhamHongTrang', 'Pham Hong Trang', 'Trangph@gmail.com', '2000-03-12', '0989989898', 'Ha Noi',
        'https://thethaobongdatoinay.files.wordpress.com/2018/11/anh-hot-girl-viet-nam-de-thuong-cuc-ky-dang-yeu4.jpg?w=1086'),
       ('GBH1709', 'PhanThiHuyenTrang', 'Phan Thi Huyen Trang', 'Trangpth@gmail.com', '2000-06-10', '0987321231',
        'Ninh Binh',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQYn4GjA5Et2mccN10W5AWNKUhQnEOs1RDmqUIyLi5jJi5HSwh7&usqp=CAU'),
       ('GBH1710', 'VuongThuyTrang', 'Vuong Thuy Trang', 'Trangvt2@gmail.com', '2000-03-04', '0987659990', 'Ha Noi',
        'https://baomuctim.com/wp-content/uploads/2018/12/kha-ngan.jpg'),
       ('GBH1711', 'NguyenVanTung', 'Nguyen Van Tung', 'Tungnv@gmail.com', '2000-03-13', '0987658787', 'Ha Noi',
        'https://www.hashatit.com/images/uploads/users/49009/profile_picture/u23_vietnam_10_zoby.jpg'),
       ('GBH1712', 'TranThiHaiYen', 'Tran Thi Hai Yen', 'Yentth@gmail.com', '2000-03-12', '0987667888', 'Ha Noi',
        'https://images.kienthuc.net.vn/zoom/800/uploaded/ctvcongdongtre/2020_01_02/4/cham-moc-30-tuoi-trong-nam-2020-dan-hot-girl-viet-nhuan-sac-den-ghen-ti.png'),
       ('GBH1713', 'NguyenThiLanAnh', 'Nguyen Thi Lan Anh', 'Anhntl@gmail.com', '2000-03-11', '0987999888', 'Ha Noi',
        'https://img.vtcnew.com.vn/files/ctv.giaoduc/2018/12/30/mai-phuong-2-11-1351367.jpg'),
       ('GBH1714', 'NghiemXuanAnh', 'Nghiem Xuan Anh', 'Anhnx@gmail.com', '2000-05-05', '0987677766', 'Ha Noi',
        'https://icdn.dantri.com.vn/thumb_w/640/2020/01/12/khoanh-khac-dang-nho-nam-2019-cua-cac-hot-boy-vietdocx-1578820342895.jpeg');
/*!40000 ALTER TABLE `student`
    ENABLE KEYS */;

-- Dumping structure for table e-tutoring.tutor
DROP TABLE IF EXISTS `tutor`;
CREATE TABLE IF NOT EXISTS `tutor`
(
    `tutor_id`      varchar(20)  NOT NULL,
    `username`      varchar(50)  NOT NULL,
    `tutor_name`    varchar(50)  NOT NULL,
    `email`         varchar(50)  NOT NULL,
    `date_of_birth` date         NOT NULL,
    `phone_number`  varchar(10)  NOT NULL,
    `address`       varchar(50)  NOT NULL,
    `avatar`        varchar(500) NOT NULL,
    PRIMARY KEY (`tutor_id`),
    KEY `FK_tutor_account` (`username`),
    CONSTRAINT `FK_tutor_account` FOREIGN KEY (`username`) REFERENCES `account` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- Dumping data for table e-tutoring.tutor: ~15 rows (approximately)
DELETE
FROM `tutor`;
/*!40000 ALTER TABLE `tutor`
    DISABLE KEYS */;
INSERT INTO `tutor` (`tutor_id`, `username`, `tutor_name`, `email`, `date_of_birth`, `phone_number`, `address`,
                     `avatar`)
VALUES ('BA001', 'NguyenPhuongDung', 'Nguyen Phuong Dung', 'Linhhntgch16483@fpt.edu.vn', '1990-01-09', '0912666688',
        'Ha Noi', 'https://whitedoctors.com.vn/wp-content/uploads/2018/07/9-600x450.jpg'),
       ('BA002', 'PhamUyenPhuongThao', 'Pham Uyen Phuong Thao', 'hieuttgch16467@fpt.edu.vn', '1990-06-07', '0998333111',
        'Ha Noi', 'https://cdnmedia.thethaovanhoa.vn/Upload/YSu1TgnVnIyxx9zisEumA/files/2019/10/0610/00259730.jpg'),
       ('BA003', 'NguyenMinhHai', 'Nguyen Minh Hai', 'haivtgch17008@fpt.edu.vn', '1990-06-10', '0932555666', 'Ha Noi',
        'https://2.bp.blogspot.com/-1lLjhSXEB6o/WEbh3iwRonI/AAAAAAAAoek/xkUp8VQNbWA1qa7Ogpy7dtby9aZi4fBuwCLcB/s1600/15327514_1839118103026397_7001401283009975522_n.jpg'),
       ('BA004', 'NguyenMinhHieu', 'Nguyen Minh Hieu', 'Hieunm@gmail.com', '1980-05-12', '0982234432', 'Ha Noi',
        'https://samsoi.net/wp-content/uploads/2018/12/ngam-bo-anh-co-giao-hotgirl-tran-thi-nam-tran-01.jpg'),
       ('BA005', 'NguyenThiNgan', 'NguyenThiNgan', 'Ngannt@gmail.com', '1980-03-16', '0387656662', 'Ha Noi',
        'https://static1.bestie.vn/Mlog/ImageContent/201810/8-tai-khoan-instagram-noi-dinh-noi-dam-cua-cac-hotgirl-viet-nam-2018-b4171d.jpg'),
       ('GD001', 'NguyenDinhTranLong', 'Nguyen Dinh Tran Long', 'Longndt@gmail.com', '1980-08-08', '0395667442',
        'Ha Noi', 'https://znews-photo.zadn.vn/w1024/Uploaded/pgi_ubnvgunau/2019_02_27/yqseungri18022019_2x.jpg'),
       ('GD002', 'TruongCongDoan', 'Truong Cong Doan', 'Doantc@gmail.com', '1980-02-20', '0987111234', 'Ha Noi',
        'https://static1.dienanh.net/360x720/upload/2019/05/08/4831ddab-e025-4e18-ade7-cfea2fd26ba1.jpg'),
       ('GD003', 'DoQuocBinh', 'Do Quoc Binh', 'Binhdq@gmail.com', '1980-08-10', '0987898343', 'Ha Noi',
        'https://static2.yan.vn/YanNews/2167221/201804/nhung-hot-boy-viet-khien-chi-em-me-man-vi-vua-dep-trai-con-tai-gioi-a01ba5cf.jpg'),
       ('GD004', 'NguyenHongPhuong', 'Nguyen Hong Phuong', 'Phuongnh@gmail.com', '1980-03-04', '0932987888', 'Ha Noi',
        'https://2sao.vietnamnetjsc.vn/images/2017/03/04/22/28/001.jpg'),
       ('GD005', 'DangThiNgocHuyen', 'Dang Thi Ngoc Huyen', 'Huyendtn@gmail.com', '1980-03-05', '0987600990', 'Ha Noi',
        'https://pbs.twimg.com/media/D0d9Zg1U8AAEXJX.jpg'),
       ('IT001', 'DoanTrungTung', 'Doan Trung Tung', 'Tungdt@gmail.com', '1980-10-16', '0989887321', 'Ha Noi',
        'https://lacongaithattuyet.vn/wp-content/uploads/2016/05/danh-sach-my-nam-han-quoc3.jpg'),
       ('IT002', 'DoHongQuan', 'Do Hong Quan', 'Quandh@gmail.com', '1990-06-15', '0987666514', 'Ha Noi',
        'https://i2.wp.com/www.nowkpop.com/wp-content/uploads/2016/08/zuho.jpg?resize=584%2C877'),
       ('IT003', 'LaiManhDung', 'Lai Manh Dung', 'Dunglm@gmail.com', '1990-05-07', '0989571864', 'Ha Noi',
        'https://i.pinimg.com/originals/68/e2/fa/68e2fab744388027a872db532ef18b66.jpg'),
       ('IT004', 'PhamThuyDuong', 'Pham Thuy Duong', 'Duongpt@gmail.com', '1988-03-13', '0912341345', 'Ha Noi',
        'https://pbs.twimg.com/media/DqEfurHUUAAxggz.jpg'),
       ('IT005', 'DoTienThanh', 'Do Tien Thanh', 'Thanhdt@gmail.com', '1988-03-10', '0980988762', 'Ha Noi',
        'https://static2.yan.vn/YanNews/2167221/201706/20170617-104935-rw_584x876.jpg');
/*!40000 ALTER TABLE `tutor`
    ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
